# Open Source License Attribution

This application uses Open Source components. You can find the source
code of their open source projects along with license information below.
We acknowledge and are grateful to these developers for their contributions
to open source.

### [go](https://github.com/golang/go)
- Copyright (c) 2009 The Go Authors. All rights reserved.
- [BSD 3-clause "New" or "Revised" License](https://github.com/golang/go/blob/master/LICENSE)

### [auth0](https://github.com/auth0-community/auth0)
- Copyright (c) 2016 Yannick Heinrich
- [MIT License](https://github.com/auth0-community/auth0/blob/v1.0.0/LICENSE)

### [jwt-go](https://github.com/dgrijalva/jwt-go)
- Copyright (c) 2012 Dave Grijalva
- [MIT License](https://github.com/dgrijalva/jwt-go/blob/v3.2.0/LICENSE)

### [gocql](https://github.com/gocql/gocql)
- Copyright (c) 2016, The Gocql authors
- [BSD 3-Clause "New" or "Revised" License](https://github.com/gocql/gocql/blob/master/LICENSE)

### [uuid](https://github.com/google/uuid)
- Copyright (c) 2009,2014 Google Inc. All rights reserved.
- [BSD 3-Clause "New" or "Revised" License](https://github.com/google/uuid/blob/v1.1.1/LICENSE)

### [handlers](https://github.com/gorilla/handlers)
- Copyright (c) 2013 The Gorilla Handlers Authors. All rights reserved.
- [BSD 2-Clause "Simplified" or "FreeBSD" license](https://github.com/gorilla/handlers/blob/v1.4.2/LICENSE)

### [mux](https://github.com/gorilla/mux)
- Copyright (c) 2012-2018 The Gorilla Authors. All rights reserved.
- [BSD 3-Clause "New" or "Revised" License](https://github.com/gorilla/mux/blob/v1.7.4/LICENSE)

### [schema](https://github.com/gorilla/schema)
- Copyright (c) 2012 Rodrigo Moraes. All rights reserved.
- [BSD 3-Clause "New" or "Revised" License](https://github.com/gorilla/schema/blob/v1.1.0/LICENSE)

### [crc16](https://github.com/howeyc/crc16)
- Copyright (c) 2012 The Go Authors. All rights reserved.
- [BSD 3-Clause "New" or "Revised" License](https://github.com/howeyc/crc16/blob/master/LICENSE)

### [cors](https://github.com/rs/cors)
- Copyright (c) 2014 Olivier Poitrey <rs@dailymotion.com>
- [MIT License](https://github.com/rs/cors/blob/v1.7.0/LICENSE)

### [go-deadlock](https://github.com/sasha-s/go-deadlock)
-       copyright notice that is included in or attached to the work
- [Apache License 2.0](https://github.com/sasha-s/go-deadlock/blob/v0.2.0/LICENSE)

### [crypto](https://golang.org/x/crypto)
- Copyright (c) 2009 The Go Authors. All rights reserved.
- [BSD 3-Clause "New" or "Revised" License](https://golang.org/x/crypto/blob/master/LICENSE)

### [sarama.v2](https://gopkg.in/Shopify/sarama.v2)
- Copyright (c) 2013 Shopify
- [MIT License](https://gopkg.in/Shopify/sarama.v2/blob/v2/LICENSE)

### [go-jose.v2](https://gopkg.in/square/go-jose.v2)
-       copyright notice that is included in or attached to the work
- [Apache License 2.0](https://gopkg.in/square/go-jose.v2/blob/v2.5.1/LICENSE)
