//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package assetmanagement

import (
	"fmt"
	"net"
	"net/http"
	"strings"

	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.com/service-platform/common"
	objsvc "gitlab.com/service-platform/services/objectmanagement"
)

var default_asset_query_options common.Query_options

type Asset_info struct {
	Name            string                 `json:"name"`
	Parent_asset_id gocql.UUID             `json:"parent_asset_id"`
	Entity_id       gocql.UUID             `json:"entity_id"`
	Id              gocql.UUID             `json:"id"`
	Description     string                 `json:"description"`
	Asset_type      gocql.UUID             `json:"asset_type"`
	Subtype         string                 `json:"subtype"`
	Collmap         string                 `json:"collmap"`
	Mobile          string                 `json:"mobile"`
	Ipaddr          net.IP                 `json:"ipaddr"`
	Kv              map[string]interface{} `json:"kv"`
	Location        string                 `json:"location"`
	Serial_no       string                 `json:"serial_no"`
	Root_asset_flag bool                   `json:"root_asset_flag"`
	Configured      bool                   `json:"configured"`
	User_defined    bool                   `json:"user_defined"`
	Tags            string                 `json:"tags"`
}

func (e *Asset_info) SetEntityId(id gocql.UUID) {
	e.Entity_id = id
}

func (e *Asset_info) GetEntityId() gocql.UUID {
	return (e.Entity_id)
}

func (e *Asset_info) SetId(id gocql.UUID) {
	e.Id = id
}

func (e *Asset_info) GetId() gocql.UUID {
	return (e.Id)
}

var Create_asset = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	var object Asset_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	common.Setup_qoptions(r, &qoptions)
	obj.Init("assets", &qoptions)

	if err := svc.Create_object_svc(w, r, &obj, &object); err != nil {
		fmt.Println(err)
	}
	return
})

var List_assets = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_asset_query_options
	common.Setup_qoptions(r, &qoptions)
	// Restricting by entity should be done through qoptions
	delete(qoptions.Where, "user_id")

	if r.Method == "POST" {
		if strings.Contains(r.URL.Path, "child-assets") {
			delete(qoptions.Where, "entity_id")
			obj.Init("assets_by_entity", &qoptions)
		} else if strings.Contains(r.URL.Path, "controlled-assets") {
			delete(qoptions.Where, "entity_id")
			obj.Init("controlled_assets", &qoptions)
		} else {
			obj.Init("assets_by_entity", &qoptions)
			//	obj.Init("assets_by_asset_type", &qoptions)
		}
	} else {
		obj.Init("assets_by_entity", &qoptions)
	}

	if err := svc.List_objects_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
})

var Get_asset = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_asset_query_options
	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	delete(qoptions.Where, "entity_id")
	obj.Init("asset", &qoptions)

	if err := svc.Get_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
	return
})

var Delete_asset = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	delete(qoptions.Where, "entity_id")
	obj.Init("assets", &qoptions)

	if err := svc.Delete_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
		http.NotFound(w, r)
		return
	}
	return
})

func SetRoutes(router *mux.Router) *mux.Router {
	s := router.PathPrefix("/assets").Subrouter()
	//s.HandleFunc("/", List_assets).Methods("GET")
	//s.HandleFunc("/list-assets", List_assets).Methods("GET")
	s.HandleFunc("/list-assets", List_assets).Methods("POST")
	s.HandleFunc("/list-controlled-assets", List_assets).Methods("POST")
	s.HandleFunc("/list-child-assets", List_assets).Methods("POST")
	s.HandleFunc("/get-asset", Get_asset).Methods("POST")
	s.HandleFunc("/delete-asset", Delete_asset).Methods("POST")
	s.HandleFunc("/create-asset", Create_asset).Methods("POST")
	s.HandleFunc("/modify-asset", Create_asset).Methods("POST")
	SettypeRoutes(s)
	/*
		router.HandleFunc("/token-auth", controllers.Login).Methods("POST")
		router.Handle("/refresh-token-auth", negroni.New(negroni.HandlerFunc(controllers.RefreshToken))).Methods("GET")
		router.Handle("/logout", negroni.New(negroni.HandlerFunc(authentication.RequireTokenAuthentication), negroni.HandlerFunc(controllers.Logout))).Methods("GET")
	*/
	return router
}

func init() {
	/*
		Where           map[string]interface{} `json:"where"`
		Groupby         string                 `json:"groupby"`
		Orderby         map[string]string      `json:"orderby"`
		Limit           int                    `json:"limit"`
		Allow_filtering bool                   `json:"allow_filtering"`
		Aggregate       map[string]interface{} `json:"aggregate"`
	*/
	default_asset_type_query_options = common.Query_options{
		Limit: 50, Allow_filtering: true}

	default_asset_query_options = common.Query_options{
		Limit: 100, Allow_filtering: true}

}
