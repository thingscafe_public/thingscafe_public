//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package assetmanagement

import (
	"fmt"
	"net/http"

	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.com/service-platform/common"
	objsvc "gitlab.com/service-platform/services/objectmanagement"
)

/* All asset types are private per entity */
type Asset_type struct {
	Name                        string                 `json:"name"`
	Description                 string                 `json:"description"`
	Id                          gocql.UUID             `json:"id"`
	Entity_id                   gocql.UUID             `json:"entity_id"`
	Controlled_by_asset_type_id gocql.UUID             `json:"controlled_by_asset_type_id"` /* Manageed by self or by another asset */
	Root_asset_flag             bool                   `json:"root_asset_flag"`
	Addressable                 bool                   `json:"addressable"`          /* Is this asset addressable and can be communicated with */
	Kind                        string                 `json:"kind"`                 /* "collection" */
	Collection_type_list        map[gocql.UUID]string  `json:"collection_type_list"` /*Collection of which types*/
	Correlation                 map[gocql.UUID]string  `json:"correlation"`          /*correlation with other assets */
	Kv                          map[string]interface{} `json:"kv"`
}

func (e *Asset_type) SetEntityId(id gocql.UUID) {
	e.Entity_id = id
}

func (e *Asset_type) GetEntityId() gocql.UUID {
	return (e.Entity_id)
}

func (e *Asset_type) SetId(id gocql.UUID) {
	e.Id = id
}

func (e *Asset_type) GetId() gocql.UUID {
	return (e.Id)
}

var Create_asset_type = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	var object Asset_type
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	obj.Init("asset_types", &qoptions)

	if err := svc.Create_object_svc(w, r, &obj, &object); err != nil {
		fmt.Println(err)
	}
	return
})

var List_asset_types = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	// TBD :Add url query options for custom-asset and controlled-asset-types

	qoptions = default_asset_type_query_options
	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	delete(qoptions.Where, "entity_id")
	obj.Init("asset_types", &qoptions)

	if err := svc.List_objects_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
})

var Get_asset_type = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//var obj asset_type_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_asset_type_query_options
	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	delete(qoptions.Where, "entity_id")
	obj.Init("asset_type", &qoptions)

	if err := svc.Get_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
	return
})

var Delete_asset_type = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	obj.Init("asset_types", &qoptions)

	if err := svc.Delete_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
		http.NotFound(w, r)
		return
	}
	http.Error(w, "Success", 200)
	return
})

func SettypeRoutes(s *mux.Router) {
	s.HandleFunc("/list-asset-types", List_asset_types).Methods("GET")
	s.HandleFunc("/list-asset-types", List_asset_types).Methods("POST")
	s.HandleFunc("/get-asset-type", Get_asset_type).Methods("POST")
	s.HandleFunc("/delete-asset-type", Delete_asset_type).Methods("POST")
	s.HandleFunc("/create-asset-type", Create_asset_type).Methods("POST")
	s.HandleFunc("/modify-asset-type", Create_asset_type).Methods("POST")
	/*
		router.HandleFunc("/token-auth", controllers.Login).Methods("POST")
		router.Handle("/refresh-token-auth", negroni.New(negroni.HandlerFunc(controllers.RefreshToken))).Methods("GET")
		router.Handle("/logout", negroni.New(negroni.HandlerFunc(authentication.RequireTokenAuthentication), negroni.HandlerFunc(controllers.Logout))).Methods("GET")
	*/
}

var default_asset_type_query_options common.Query_options
