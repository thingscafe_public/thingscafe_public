//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package configmanagement

import (
	"fmt"
	"net/http"

	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.com/service-platform/common"
	objsvc "gitlab.com/service-platform/services/objectmanagement"
)

/*
 * Config Schema contain schema for a certain kind of profile
 * described in the schema below
 */
type Config_schema struct {
	Name                string                 `json:"name"` /* Schema name */
	Description         string                 `json:"description"`
	Id                  gocql.UUID             `json:"id"`                /* Schema id */
	Entity_id           gocql.UUID             `json:"entity_id"`         /* Schema are per SAAS provider */
	Cschema_type_id     gocql.UUID             `json:"cschema_type_id"`   /* to which asset_type this applies */
	Cschema_type_name   string                 `json:"cschema_type_name"` /* to which asset_type this applies */
	Cschema_type        string                 `json:"cschema_type"`      /* owner_config or config*/
	Cschema_json        string                 `json:"cschema_json"`      /* Json input for rendering  */
	Apply_schema_id     gocql.UUID             `json:"apply_schema_id"`   /* Apply Schema id */
	Apply_schema_name   string                 `json:"apply_schema_name"` /* Apply Schema id */
	Config_order        int                    `json:"config_order"`      /* Apply Schema id */
	Kv                  map[string]interface{} `json:"kv"`
	Allowed_entity_type int                    `json:"allowed_entity_type"`
}

func (e *Config_schema) SetEntityId(id gocql.UUID) {
	e.Entity_id = id
}

func (e *Config_schema) GetEntityId() gocql.UUID {
	return (e.Entity_id)
}

func (e *Config_schema) SetId(id gocql.UUID) {
	e.Id = id
}

func (e *Config_schema) GetId() gocql.UUID {
	return (e.Id)
}

var Create_config_schema = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	var object Config_schema
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	obj.Init("config_schemas", &qoptions)

	if err := svc.Create_object_svc(w, r, &obj, &object); err != nil {
		fmt.Println(err)
	}
	return
})

var List_config_schemas = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_config_schema_query_options
	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	delete(qoptions.Where, "entity_id")
	obj.Init("config_schemas", &qoptions)

	if err := svc.List_objects_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
})

var Get_config_schema = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//var obj config_schema_info
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	qoptions = default_config_schema_query_options
	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	delete(qoptions.Where, "entity_id")
	obj.Init("config_schemas", &qoptions)

	if err := svc.Get_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
	}
	return
})

var Delete_config_schema = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	var svc objsvc.Generic_svc
	var obj common.Object_handler
	var qoptions common.Query_options

	common.Setup_qoptions(r, &qoptions)
	delete(qoptions.Where, "user_id")
	obj.Init("config_schemas", &qoptions)

	if err := svc.Delete_object_svc(w, r, &obj); err != nil {
		fmt.Println(err)
		http.NotFound(w, r)
		return
	}
	http.Error(w, "Success", 200)
	return
})

func SetschemaRoutes(s *mux.Router) {
	s.HandleFunc("/list-schemas", List_config_schemas).Methods("GET")
	s.HandleFunc("/get-schema", Get_config_schema).Methods("POST")
	s.HandleFunc("/delete-schema", Delete_config_schema).Methods("POST")
	s.HandleFunc("/create-schema", Create_config_schema).Methods("POST")
	s.HandleFunc("/modify-schema", Create_config_schema).Methods("POST")
	/*
		router.HandleFunc("/token-auth", controllers.Login).Methods("POST")
		router.Handle("/refresh-token-auth", negroni.New(negroni.HandlerFunc(controllers.RefreshToken))).Methods("GET")
		router.Handle("/logout", negroni.New(negroni.HandlerFunc(authentication.RequireTokenAuthentication), negroni.HandlerFunc(controllers.Logout))).Methods("GET")
	*/
}

var default_config_schema_query_options common.Query_options
