#!/bin/bash
#   Copyright 2020 thingscafe.net
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http:#www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

plugins=saasv
BUILD=${TCBUILD}
mkdir $BUILD/plugins
set -x
for plugin in $plugins
do
	echo $plugin
	cd $plugin
	p="${plugin}_process.so"
	go clean -x 
	go get
	go build -buildmode=plugin -o $p
	if [ "$?" -eq 0 ]
	then
		cp $p $BUILD/plugins/
		echo "Built $p"
		ls -l $BUILD/plugins/$p
	else
		echo "Build failed"
	fi
	cd ..
done
