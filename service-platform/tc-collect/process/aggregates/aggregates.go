//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package aggregates

import (
	"sync"
	"time"
)

// Post specific processing by plugins.
// Normalized into events and stats

type aggregate struct {
	Min         interface{} `json:"min"`
	Max         interface{} `json:"max"`
	Abs         interface{} `json:"abs"`
	Avg         float64     `json:"avg"`
	Sum         float64     `json:"sum"`
	Count       uint64      `json:"count"`
	Rate        float64     `json:"rate"`
	Stat_type   string      `json:"stat_type"`
	resource_id string
	last_value  interface{}
	last_count  uint64
	ts          time.Time
	mux         sync.Mutex
	state       string
}

type Metadata struct {
	Resource_id string                 `json:"resource_id"`
	Entity_name string                 `json:"entity_name"`
	Asset_name  string                 `json:"asset_name"`
	App         string                 `json:"app"`
	Kv          map[string]interface{} `json:"kv"`
	flag        string
}

type Stats_record struct {
	Resource_id          string                 `json:"resource_id"`
	Entity_name          string                 `json:"entity_name"`
	Asset_name           string                 `json:"asset_name"`
	Aggregation_type     string                 `json:"aggregation_type"`
	Ts                   int64                  `json:"ts"`
	Aggregation_table    map[string]aggregate   `json:"aggregation_table"`
	Aggregation_interval int64                  `json:"aggregation_interval"`
	Kv                   map[string]interface{} `json:"kv"`
	app                  string
	//	Cnts1h               map[string]aggregate   `json:"cnts1h"`
	//	Cnts1day             map[string]aggregate   `json:"cnts1day"`
	//	Cnts1week            map[string]aggregate   `json:"cnts1week"`
	//	Cnts1month           map[string]aggregate   `json:"cnts1month"`
	//Stat_type   string                 `json:"stat_type"`
	//Seq         int    `json:"seq"`
}

func typeof(v interface{}) string {
	switch v.(type) {
	case int64:
		return "int64"
	case uint64:
		return "uint64"
	case float64:
		return "float64"
		//... etc
	default:
		return "unknown"
	}
}

func (aggr *aggregate) Initialize(cntr_type string, flag string) {
	aggr.mux.Lock()
	if cntr_type == "gauge" {
		aggr.Min = int64(0)
		aggr.Max = int64(0)
		aggr.Abs = int64(0)
		aggr.last_value = int64(0)
	} else {
		aggr.Min = uint64(0)
		aggr.Max = uint64(0)
		aggr.Abs = uint64(0)
		aggr.last_value = uint64(0)
	}
	aggr.Sum = float64(0)
	aggr.Count = 0
	aggr.Avg = float64(0)
	aggr.Rate = float64(0)
	aggr.Stat_type = cntr_type
	aggr.ts = time.Now()
	aggr.last_count = uint64(0)
	if flag == "" {
		aggr.state = "init"
	} else {
		aggr.state = flag
	}
	aggr.mux.Unlock()
}

func (aggr *aggregate) Copy(raggr *aggregate) {

	aggr.Initialize(raggr.Stat_type, "restored")
	*aggr = *raggr
	aggr.state = "restored"

	if raggr.Stat_type == "gauge" {
		////
		aggr.Max = int64(raggr.Max.(float64))
		aggr.Min = int64(raggr.Min.(float64))
		aggr.Abs = int64(raggr.Abs.(float64))
		aggr.last_value = aggr.Abs
	} else {
		aggr.Max = uint64(raggr.Max.(float64))
		aggr.Min = uint64(raggr.Min.(float64))
		aggr.Abs = uint64(raggr.Abs.(float64))
		aggr.last_value = aggr.Abs
	}
	aggr.ts = time.Now()
}

func (aggr *aggregate) Rollup(raggr *aggregate) {

	atype := typeof(aggr.last_value)

	if raggr.Stat_type == "gauge" && atype == "uint64" ||
		raggr.Stat_type == "counter" && atype == "int64" {
		// If type of stat changed, just ignore the old data and re-initialize
		aggr.Initialize(raggr.Stat_type, "reset")
	}
	aggr.mux.Lock()
	raggr.mux.Lock()

	if raggr.Stat_type == "gauge" {
		aggr.last_value = raggr.last_value
		aggr.Max = aggr.FindMax(raggr.Max)
		aggr.Min = aggr.FindMin(raggr.Min)
		aggr.Abs = raggr.Abs
	} else {
		v1 := raggr.last_value.(uint64)
		if aggr.last_value != nil {
			if aggr.last_value.(uint64) < v1 {
				v1 = v1 - aggr.last_value.(uint64)
			}
		}
		elapsed := uint64(time.Since(aggr.ts).Seconds())
		if elapsed > 0 {
			aggr.Rate = float64(v1 / elapsed)
		}
		aggr.Max = aggr.FindMax(raggr.Max)
		aggr.Min = aggr.FindMin(raggr.Min)
		aggr.last_value = raggr.last_value
		aggr.Abs = raggr.Abs
	}
	aggr.Count += raggr.Count
	aggr.Sum += raggr.Sum
	raggr.mux.Unlock()

	aggr.Avg = aggr.Sum / float64(aggr.Count)
	aggr.ts = time.Now()
	aggr.state = "updated"
	aggr.mux.Unlock()
}

func (aggr *aggregate) Setstate(state string) {
	aggr.state = state
}

func (aggr *aggregate) Getstate() string {
	return aggr.state
}

func (aggr *aggregate) Setvalues(value interface{}) {
	aggr.mux.Lock()
	if aggr.Stat_type == "gauge" {
		v := value.(int64)
		aggr.Sum += float64(v)
		aggr.last_value = v
		aggr.Max = aggr.FindMax(v)
		aggr.Min = aggr.FindMin(v)
		aggr.Abs = v
	} else if aggr.Stat_type == "counter" {
		v := value.(uint64)
		v1 := v
		if aggr.last_value.(uint64) < v {
			v1 = v - aggr.last_value.(uint64)
		}
		aggr.Sum += float64(v1)
		elapsed := uint64(time.Since(aggr.ts).Seconds())
		if elapsed > 0 {
			aggr.Rate = float64(v1 / elapsed)
		}
		aggr.Max = aggr.FindMax(v1)
		aggr.Min = aggr.FindMin(v1)
		aggr.last_value = v
		aggr.Abs = v
	}
	aggr.Count++
	aggr.Avg = aggr.Sum / float64(aggr.Count)
	aggr.ts = time.Now()
	aggr.state = "updated"
	aggr.mux.Unlock()
}

func (aggr *aggregate) Setmax(y interface{}) {
	aggr.Max = aggr.FindMax(y)
}

func (aggr *aggregate) Setmin(y interface{}) {
	aggr.Min = aggr.FindMin(y)
}

// Min returns the smaller of x or y.
func (aggr *aggregate) FindMin(y interface{}) interface{} {
	if aggr.Stat_type == "gauge" {
		if aggr.Min == int64(0) {
			return y
		}
		if aggr.Min.(int64) > y.(int64) {
			return y
		}
	} else {
		if aggr.Min == uint64(0) {
			return y
		}
		if aggr.Min.(uint64) > y.(uint64) {
			return y
		}
	}
	return aggr.Min
}

func (aggr *aggregate) FindMax(y interface{}) interface{} {
	if aggr.Stat_type == "gauge" {
		if aggr.Max.(int64) < y.(int64) {
			return y
		}
	} else {
		if aggr.Max.(uint64) < y.(uint64) {
			return y
		}
	}
	return aggr.Max
}

func (aggr *aggregate) Umax(y uint64) interface{} {
	if aggr.Max.(uint64) < y {
		return y
	}
	return aggr.Max
}

// Min returns the smaller of x or y.
func (aggr *aggregate) Umin(y uint64) interface{} {
	if aggr.Min.(uint64) > y {
		return y
	}
	return aggr.Min
}
