This directory contains all the code related to the tc-collect service.

This service is used to receive different types of data from the controller.
Currently, it supports events/alarms/stats collection. These can be enhanced by
adding additional collectors.  

At the end of the processing, events/alarms/stats are stored in the DB 
for querying and/or published on a Kafka topic for consumption by other
services including 3rd party services.
