//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package main

import (
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	mykafka "gitlab.com/service-platform/common/mykafka"
	"gitlab.com/service-platform/tc-mw/utils"
)

//var logger = log.With().Str("pkg", "main").Logger()
/*
	Utilities
*/

// Get the port to listen on
func getListenAddress() string {
	port := utils.Getenv("COLLECTORPORT", "8080")
	return ":" + port
}

func main() {

	addr1 := getListenAddress()
	r1 := mux.NewRouter()
	r := r1.PathPrefix("/tccollect/").Subrouter()
	r = SetRoutes(r)
	logRouter := handlers.LoggingHandler(os.Stdout, r1)
	//r.Use(loggingMiddleware)
	//r.Use(basicauth.BasicAuth)

	//err := mykafka.Init()
	//if err != nil {
	//	log.Fatal("Cannot connect to Kafka")
	//}

	kc := utils.Getenv("COLLECTOR_CLIENT", "tc_collector")
	np := utils.Getenv("COLLECTOR_NUM_PARTITIONS", "8")
	num_part, _ := strconv.Atoi(np)
	_ = num_part

	mykafka.InitProducer()
	h := mykafka.GetProducer(kc)
	if h == nil {
		log.Fatal("Cannot create Producer")
	}

	Set_kafka_handle(h)
	// Trap SIGINT to trigger a graceful shutdown.
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)

	//defer conn.Close()
	// Initialize interfaces for service to acces DB
	//log.Fatal(http.ListenAndServe(":8080", r))
	log.Printf("starting Collector", addr1)
	serv := &http.Server{
		Addr:           addr1,
		Handler:        logRouter,
		ReadTimeout:    30 * time.Second,
		WriteTimeout:   30 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	go func() {
		log.Fatal(serv.ListenAndServeTLS("server.pem", "server.key"))
	}()
	<-signals
	log.Println("\n Shutting down Collector")

	//mykafka.Close()
	mykafka.Close(h)
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Do stuff here
		log.Println(r.RequestURI)
		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}
