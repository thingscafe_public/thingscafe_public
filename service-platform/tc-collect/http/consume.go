//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	mykafka "gitlab.com/service-platform/common/mykafka"
)

var decoder = schema.NewDecoder()

/*
var Consume_data_org = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	parent := context.Background()
	defer parent.Done()
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	codec, err := goavro.NewCodec(`
            {
              "type": "record",
              "name": "credentials",
              "fields" : [
		  {"name": "id", "type": "string", "logical_type" : "uuid"},
		  {"name": "password", "type": "string"}
              ]
            }`)
	if err != nil {
		fmt.Println(err)
	}

	// Convert textual Avro data (in Avro JSON format) to native Go form
	native, _, err := codec.NativeFromTextual(body)
	if err != nil {
		fmt.Println(err)
	}
	if err != nil {
		return
	}
	// Avro ??
	//Convert native Go form to binary Avro data
	binary, err := codec.BinaryFromNative(nil, native)
	if err != nil {
		fmt.Println(err)
	}
	err = mykafka.Push(parent, nil, binary)
})
*/

type query_string struct {
	App   string `schema:"app"`
	Ename string `schema:"ename"`
	Cid   string `schema:"cid"`
}

var Kafka_handle interface{}

var Consume_data = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	parent := context.Background()
	defer parent.Done()

	err := r.ParseForm()

	if err != nil {
		log.Println("Received Message Format error", err.Error(), r.URL, r.Form, r.PostForm)
		http.Error(w, err.Error(), 500)
		return
	} else {
		log.Println("Received Message :", r.URL, r.Form)
	}

	qdata := new(query_string)

	err = decoder.Decode(qdata, r.Form)

	if err != nil {
		log.Println("Received Message Decoding Error", err.Error(), r.URL, r.Form, r.PostForm)
		http.Error(w, err.Error(), 500)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	var dat map[string]interface{}
	err = json.Unmarshal(body, &dat)
	if err != nil {
		log.Printf("Unmarshalling Error:%s Body:%s", err, string(body))
		http.Error(w, err.Error(), 500)
		return
	}
	// Add the variables from query string to the json
	dat["tcafe"+"_"+"app"] = qdata.App
	dat["tcafe"+"_"+"cid"] = qdata.Cid
	dat["tcafe"+"_"+"ename"] = qdata.Ename

	t, err := json.Marshal(dat)
	if err != nil {
		log.Println("Error while augmenting:", err)
		http.Error(w, err.Error(), 500)
		return
	}
	log.Println(string(t))
	//kafkaProducer, err := mykafka.Get_conn(strings.Split(kafkaBrokerUrl, ","), kafkaClientId, kafkaTopic)

	//err = mykafka.Push(parent, nil, t)
	/*
		pid := rand.Intn(mykafka.Kafka_num_partitions)
		kafkawriter, err := mykafka.Getconn(mykafka.KafkaBrokerUrls, mykafka.KafkaClientId, qdata.App, pid, mykafka.Kafka_num_partitions)
		//kafkaconn, err := mykafka.Getconn(strings.Split(mykafka.KafkaBrokerUrl, ","), mykafka.KafkaClientId, qdata.App, pid, mykafka.Kafka_num_partitions)
		//err = mykafka.Push_to_conn(kafkaconn, []byte(qdata.Cid), t)
		//defer kafkawriter.Close()
		err = mykafka.Push(kafkawriter, parent, []byte(qdata.Cid), t)
	*/
	topic := qdata.App + "_data"
	if !mykafka.Topic_exists(Kafka_handle, topic) {
		tc := mykafka.Get_topic_config(topic)
		err := mykafka.Create_topic(Kafka_handle, topic, tc)
		if err != nil {
			log.Println("Cannot create topic :", topic, err)
			return
		}
	}

	mykafka.Push(Kafka_handle, topic, qdata.Cid, t)
	//defer kafkaconn.Close()
})

func Set_kafka_handle(h interface{}) {
	Kafka_handle = h
}

func SetRoutes(router *mux.Router) *mux.Router {
	s := router.PathPrefix("/v1/").Subrouter()
	/*
		s.HandleFunc("/", List_assets).Methods("GET")
		s.HandleFunc("/list-assets", List_assets).Methods("GET")
		s.HandleFunc("/get-asset", Get_asset).Methods("POST")
		s.HandleFunc("/delete-asset", Delete_asset).Methods("POST")
		s.HandleFunc("/create-asset", Create_asset).Methods("POST")
	*/
	s.HandleFunc("/submit-data", Consume_data).Methods("POST")
	return router
}

func init() {

}
