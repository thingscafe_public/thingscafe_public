//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package mykafka

import (
	"context"
	"log"
	"os"

	sarama "gopkg.in/Shopify/sarama.v2"
	//"github.com/Shopify/sarama"
)

// Sarma configuration options

type Consumerg_handle interface {
	Consume(ctx context.Context, topics []string, handler sarama.ConsumerGroupHandler) error
	Close()
	Find_last_offset(topic string) (int32, int64)
}

type consumerg_handle struct {
	client sarama.Client
	cg     sarama.ConsumerGroup
}

func InitConsumerg(group string, a string, c string, offset int64) Consumerg_handle {
	var (
		client_id = ""
		assignor  = ""
	)

	if group == "" {
		log.Panicf("no Kafka consumer group defined")
	}

	if a == "" {
		assignor = "range"
	} else {
		assignor = a
	}

	client_id = c

	//vers := "2.3.1"
	vers := "2.3.1"

	log.Println("Starting a new Sarama consumer")

	if KafkaVerbose {
		sarama.Logger = log.New(os.Stdout, "[sarama] ", log.LstdFlags)
	}

	version, err := sarama.ParseKafkaVersion(vers)
	if err != nil {
		log.Panicf("Error parsing Kafka version: %v", err)
	}

	/**
	 * Construct a new Sarama configuration.
	 * The Kafka cluster version has to be defined before the consumer/producer is initialized.
	 */
	config := sarama.NewConfig()
	config.Version = version
	config.ClientID = client_id

	switch assignor {
	case "roundrobin":
		config.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRoundRobin
	case "range":
		config.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRange
	default:
		log.Panicf("Unrecognized consumer group partition assignor: %s", assignor)
	}

	config.Consumer.Offsets.Initial = offset

	client, err := sarama.NewClient(KafkaBrokerUrls, config)
	if err != nil {
		log.Panicf("Error creating Kafka client")
	}
	cg, err := sarama.NewConsumerGroupFromClient(group, client)
	if err != nil {
		log.Panicf("Error creating Kafka Consumer group", group)
	}

	cg_handle := new(consumerg_handle)
	cg_handle.cg = cg
	cg_handle.client = client

	return cg_handle
}

func (cgh *consumerg_handle) Find_last_offset(topic string) (int32, int64) {

	var offset int64 = -1
	var part int32 = -1
	var parts []int32
	var err error

	parts, err = cgh.client.Partitions(topic)

	if err != nil {
		return -1, -1
	}

	for pid := range parts {
		off, err := cgh.client.GetOffset(topic, int32(pid), sarama.OffsetNewest)
		if err == nil {
			if offset < off {
				offset = off
				part = int32(pid)
			}
		}
	}
	return part, offset - 1
}

func (cgh *consumerg_handle) Consume(ctx context.Context, topics []string, handler sarama.ConsumerGroupHandler) error {

	cg := cgh.cg

	return cg.Consume(ctx, topics, handler)
}

func (cgh *consumerg_handle) Close() {
	cg := cgh.cg
	client := cgh.client

	cg.Close()
	client.Close()
}
