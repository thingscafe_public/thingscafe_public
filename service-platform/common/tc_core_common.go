//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package common

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/gocql/gocql"
	"golang.org/x/crypto/bcrypt"
)

type ContextValues struct {
	M map[string]interface{}
}

func (v ContextValues) Get(key string) interface{} {
	return v.M[key]
}

type emptyI interface {
	SetId(id gocql.UUID)
	GetId() (id gocql.UUID)
	SetEntityId(id gocql.UUID)
	GetEntityId() (id gocql.UUID)
}

type ContextKey string
type Password string

const ContextUserKey ContextKey = "user"
const ContextEntityKey ContextKey = "entity"
const SaasEntityKey ContextKey = "saasentity"
const ContextEntityTypeKey ContextKey = "entity_type"
const ContextRoleKey ContextKey = "user_role"

const RootAssetTypeId string = "195a24a2-0aad-11e9-ab14-d663bd873d93"

type Entity_type int
type Relationship int
type User_role int

type Query_options struct {
	Where           map[string]interface{} `json:"where"`
	Contains_flag   bool                   `json:"contains_flag"`
	Aggregate       map[string]interface{} `json:"aggregate"`
	Groupby         string                 `json:"groupby"`
	Orderby         map[string]string      `json:"orderby"`
	Limit           int                    `json:"limit"`
	Allow_filtering bool                   `json:"allow_filtering"`
	Pagesize        int
	Where_complex   []Query_where
	Internal        map[string]interface{}
}

type Filter_options struct {
	Groupby         string            `json:"groupby"`
	Orderby         map[string]string `json:"orderby"`
	Limit           int               `json:"limit"`
	Allow_filtering bool              `json:"allow_filtering"`
}

type Query_where struct {
	Operator string
	Lhs      string
	Rhs      interface{}
}

type Update_options struct {
	Set           map[string]interface{} `json:"set"`
	Where         map[string]interface{} `json:"where"`
	Contains_flag bool                   `json:"contains_flag"`
	In_flag       bool                   `json:"in_flag"`
	Ifexists_flag bool                   `json:"ifexists_flag"`
	Ttl           int                    `json:"ttl"`
	Timestamp     int                    `json:"timestamp"`
	Operation     string                 `json:"operation"`
}

func (obj *Object_handler) Get_entity() (id gocql.UUID, result bool) {
	if len(obj.Entity) > 0 {
		return obj.Entity, true
	} else {
		return gocql.UUID{}, false
	}
}

func (obj *Object_handler) Get_modify_id() (id gocql.UUID, result bool) {
	/* Entity-id or user-id from user credentials should be used during modify workflow */

	//if obj.object_type == "user" || obj.object_type == "user_credentials" {
	if obj.object_type == "user" {
		id = obj.Qoptions.Where["user_id"].(gocql.UUID)
		return id, true
	}
	return gocql.UUID{}, false
}

func Setup_qoptions_with_etype(r *http.Request, qoptions *Query_options) Entity_type {
	ctx := r.Context()
	Setup_qoptions(r, qoptions)

	my_type := ctx.Value("MyContextvalues").(ContextValues).Get(string(ContextEntityTypeKey))

	if my_type != nil {
		my_entity_type, err := strconv.Atoi(my_type.(string))
		if err == nil {
			return Entity_type(my_entity_type)
		} else {
			return -1
		}
	}
	return -1
}

func Setup_qoptions(r *http.Request, qoptions *Query_options) {

	ctx := r.Context()
	//restrict_by_entity := ctx.Value("MyContextvalues").(ContextValues).Get(string(ContextEntityKey)).(gocql.UUID)
	saas_entity := ctx.Value("MyContextvalues").(ContextValues).Get(string(SaasEntityKey))
	restrict_by_entity := ctx.Value("MyContextvalues").(ContextValues).Get(string(ContextEntityKey))
	restrict_by_user := ctx.Value("MyContextvalues").(ContextValues).Get(string(ContextUserKey))
	qoptions.Internal = make(map[string]interface{})
	qoptions.Where = make(map[string]interface{})

	qoptions.Where["saas_entity_name"] = saas_entity
	qoptions.Where["keyspace"] = saas_entity

	qoptions.Internal["keyspace"] = saas_entity

	qoptions.Internal["user"] = restrict_by_user
	qoptions.Internal["entity"] = restrict_by_entity

	entity_id := restrict_by_entity.(emptyI).GetId()
	qoptions.Where["entity_id"] = entity_id
	user_id := restrict_by_user.(emptyI).GetId()
	qoptions.Where["user_id"] = user_id
}

func Check_against_user_entity(r *http.Request, id interface{}) bool {

	ctx := r.Context()
	restrict_by_entity := ctx.Value("MyContextvalues").(ContextValues).Get(string(ContextEntityKey)).(gocql.UUID)

	if id == restrict_by_entity {
		return true
	}
	return false
}

type Object interface {
	Initold(object_type interface{})
	Init(object_type interface{}, options interface{})
	Init_for_update(object_type interface{}, options interface{})
	Unmarshal(r *http.Request, obj interface{}) (error, map[string]*json.RawMessage, []byte)
	Marshal(r *http.Request) error
	Write_permissions(w http.ResponseWriter, r *http.Request) error
	Read_permissions(w http.ResponseWriter, r *http.Request) error

	//Add_additional_context(w http.ResponseWriter, r *http.Request) error
	Get_modify_id() (id gocql.UUID, result bool)
	Get_entity() (id gocql.UUID, result bool)
	Get_table() string
	Get_type() string
	Get_qoptions() *Query_options
	Get_uoptions() *Update_options
	Get_serviced_entity_flag() bool
	Set_keyspace(object_type interface{})
}

type Object_handler struct {
	Id                   gocql.UUID     `json:"id"`
	Name                 string         `json:"name"`
	Entity               gocql.UUID     `json:"entity_id"`
	Qoptions             Query_options  `json:"qoptions"`
	Uoptions             Update_options `json:"uoptions"`
	Check_entity         bool
	Serviced_entity_flag bool
	object_type          interface{}
}

func (obj *Object_handler) Initold(object_type interface{}) {
	obj.object_type = object_type
}

func (obj *Object_handler) Init(object_type interface{}, options interface{}) {
	obj.object_type = object_type
	obj.Qoptions = *(options.(*Query_options))
}

func (obj *Object_handler) Init_for_update(object_type interface{}, options interface{}) {
	obj.object_type = object_type
	obj.Uoptions = *(options.(*Update_options))
}

func (obj *Object_handler) Set_keyspace(object_type interface{}) {
	obj.Qoptions.Where["keyspace"] = object_type.(string)
}

func Get_saas_entity(qoptions *Query_options) string {
	saas := (qoptions.Where["saas_entity_name"]).(string)
	return saas
}

func (obj *Object_handler) Get_table() string {
	keyspace := (obj.Qoptions.Where["keyspace"]).(string)
	delete(obj.Qoptions.Where, "keyspace")
	delete(obj.Qoptions.Where, "saas_entity_name")
	return keyspace + "." + (obj.object_type).(string)
}

func (obj *Object_handler) Get_qoptions() *Query_options {
	return &(obj.Qoptions)
}
func (obj *Object_handler) Get_uoptions() *Update_options {
	return &(obj.Uoptions)
}

func (obj *Object_handler) Get_type() string {
	return (obj.object_type).(string)
}

func (obj *Object_handler) Get_serviced_entity_flag() bool {
	return (obj.Serviced_entity_flag)
}

func (obj *Object_handler) Unmarshal(r *http.Request, object interface{}) (error, map[string]*json.RawMessage, []byte) {
	var objmap map[string]*json.RawMessage
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		return err, nil, body
	}
	log.Println(string(body))
	err = json.Unmarshal(body, &objmap)
	if err != nil {
		return err, nil, body
	}
	if obj != object {
		err = json.Unmarshal(body, object)
	} else {
		err = json.Unmarshal(body, obj)
	}
	return err, objmap, body
}

func (obj *Object_handler) Marshal(r *http.Request) error {
	return nil
}

func (obj *Object_handler) Write_permissions(w http.ResponseWriter, r *http.Request) error {
	err := errors.New("No write permission")
	if !Write_permissions(w, r) {
		return err
	}
	return nil
}

func (obj *Object_handler) Read_permissions(w http.ResponseWriter, r *http.Request) error {
	err := errors.New("No read permission")
	if !Read_permissions(w, r) {
		return err
	}
	return nil
}

/*
func (obj *Object_handler) Add_additional_context(w http.ResponseWriter, r *http.Request) error {
	return nil
}
*/

const (
	INSTALLER Entity_type = iota
	SAASPROVIDER
	SERVICEPROVIDER
	CUSTOMER
	SITE
	MISCELLANEOUS
	UNKNOWN
)

const (
	peer Relationship = iota
	parent
	ancestor
	child
	descendant
	unknown
)

const (
	Administrator User_role = iota
	Operator
	Unknown
)

/*
 * TBD Uncomment this when we find how to avoid Marshaling before storing into DB
func (etype *Entity_type) MarshalJSON() ([]byte, error) {
	buffer := bytes.NewBufferString("")
	s := etype.String()
	jsonValue, _ := json.Marshal(s)
	//	buffer.WriteString(fmt.Sprintf("\"%d\":%s", key, string(jsonValue)))
	buffer.WriteString(fmt.Sprintf("%s", string(jsonValue)))
	//buffer.WriteString("}")
	return buffer.Bytes(), nil
}
*/
func (urole *User_role) UnmarshalJSON(b []byte) error {
	var stuff string
	_ = json.Unmarshal(b, &stuff)

	*urole = urole.Role(stuff)
	return nil
}

func (etype *Entity_type) UnmarshalJSON(b []byte) error {
	var stuff string
	_ = json.Unmarshal(b, &stuff)

	*etype = etype.Entity(stuff)
	return nil
}

func (etype *Entity_type) String() string {

	types := [...]string{
		"INSTALLER",
		"SAASPROVIDER",
		"SERVICEPROVIDER",
		"CUSTOMER",
		"SITE",
		"MISCELLANEOUS",
		"UNKNOWN"}

	return types[*etype]
}

func (e *Entity_type) Entity(entity string) Entity_type {

	types := [...]string{
		"INSTALLER",
		"SAASPROVIDER",
		"SERVICEPROVIDER",
		"CUSTOMER",
		"SITE",
		"MISCELLANEOUS",
		"UNKNOWN"}

	for r := range types {

		if entity == types[r] {
			return Entity_type(r)
		}
	}
	return Entity_type(UNKNOWN)
}

func (r User_role) String() string {

	roles := [...]string{
		"ADMINISTRATOR",
		"OPERATOR"}

	return roles[r]
}

func (r User_role) Role(role string) User_role {

	roles := [...]string{
		"ADMINISTRATOR",
		"OPERATOR",
		"UNKNOWN"}

	for r := range roles {

		if role == roles[r] {
			return User_role(r)
		}
	}
	return User_role(Unknown)
}

func (etype1 Entity_type) related(etype2 Entity_type) (Relationship, string) {

	types := [...]string{
		"peer",
		"parent",
		"ancestor",
		"child",
		"descendant"}

	val := etype1 - etype2

	switch val {
	case 0:
		return peer, types[peer]
	case -1:
		return parent, types[parent]
	case 1:
		return child, types[child]
	default:
		if val < -1 {
			return ancestor, types[ancestor]
		}
		if val > 1 {
			return descendant, types[descendant]
		}
	}
	return peer, types[peer]
}

/* Check if the user has write permissions based on user role */
/* If role does not allow creation/write, bail out here */
/* Also get entity_type of Entity which user belongs to and pass it down in context */
/* Entity_type can be used to check rules later for any calls made */

func Write_permissions(w http.ResponseWriter, r *http.Request) bool {
	ctx := r.Context()
	restrict_by_role := ctx.Value("MyContextvalues").(ContextValues).Get(string(ContextRoleKey))
	if restrict_by_role != Administrator {
		http.Error(w, "No permission to create", http.StatusUnauthorized)
		return false
	}
	return true
}

/* Check if the user has read permissions based on user role */
/* Also get entity_type of Entity which user belongs to and pass it down in context */
/* Entity_type can be used to check rules later for any calls made */

func Read_permissions(w http.ResponseWriter, r *http.Request) bool {
	ctx := r.Context()
	restrict_by_role := ctx.Value("MyContextvalues").(ContextValues).Get(string(ContextRoleKey))
	if restrict_by_role != Operator &&
		restrict_by_role != Administrator {
		http.Error(w, "No permission to read", http.StatusUnauthorized)
		return false
	}
	return true
}

func (e *Password) UnmarshalJSON(b []byte) error {
	var stuff string
	_ = json.Unmarshal(b, &stuff)
	*e = Password(HashAndSalt([]byte(stuff)))
	return nil
}

func HashAndSalt(pwd []byte) string {

	// Use GenerateFromPassword to hash & salt pwd.
	// MinCost is just an integer constant provided by the bcrypt
	// package along with DefaultCost & MaxCost.
	// The cost can be any value you want provided it isn't lower
	// than the MinCost (4)
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	// GenerateFromPassword returns a byte slice so we need to
	// convert the bytes to a string and return it
	return string(hash)
}
