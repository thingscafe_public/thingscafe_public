//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package cooked

import "time"

// Post specific processing by plugins.
// Normalized into events and stats
type Cooked_data struct {
	Events []Event_record
	Stats  []Stats_record
}

// Used to create threshold alarms
// Plugin Init to return a map with the counter/gauge name and thresholds
// Example :var threshold_map map[string]thresholds
// thres
//	threshold_map = make(map[string]thresholds)
//  threshold_map["temp"].min = 15
//  threshold_map["temp"].max = 40
//  threshold_map["temp"].number_of_times = 3
//  threshold_map["temp"].number_of_times = "avg"
//
type Thresholds struct {
	Aggregate_type  string //avg, rate, min, max, count, value
	Min             uint64
	Max             uint64
	Number_of_times int           // exceeded number of times consecutively
	Period          time.Duration // period for which to check
}

type Event_record struct {
	Resource    string                 `json:"resource"`
	Seq         int                    `json:"seq"`
	Day         string                 `json:"day"`
	Ts          int64                  `json:"ts"`
	Asset_type  string                 `json:"asset_type"`
	Asset_name  string                 `json:"asset_name"`
	Entity_name string                 `json:"entity_name"`
	Description string                 `json:"description"`
	Severity    string                 `json:"severity"`
	Details     string                 `json:"details"`
	App         string                 `json:"app"`
	Kv          map[string]interface{} `json:"kv"`
}

type Stats_record struct {
	Resource    string                 `json:"resource"`
	Seq         int                    `json:"seq"`
	Ts          int64                  `json:"ts"`
	Asset_type  string                 `json:"asset_type"`
	Asset_name  string                 `json:"asset_name"`
	Entity_name string                 `json:"entity_name"`
	App         string                 `json:"app"`
	Gauges      map[string]int64       `json:"gauges"`
	Counters    map[string]uint64      `json:"counters"`
	Kv          map[string]interface{} `json:"kv"`
}
