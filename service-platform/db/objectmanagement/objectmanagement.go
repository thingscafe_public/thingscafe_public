//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package objectmanagement

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"

	"github.com/gocql/gocql"
	"gitlab.com/service-platform/common"
	"gitlab.com/service-platform/db/Cassandra"
	//	uuid "github.com/satori/go.uuid"
)

type DBObject interface {
	Create_object(ctx context.Context, create_json []byte, object common.Object) error
	List_objects(ctx context.Context, list_json []byte, object common.Object) ([]byte, error)
	List_paged_objects(ctx context.Context, list_json []byte, object common.Object, pgstate interface{}) ([]byte, error, interface{})
	Delete_object(ctx context.Context, delete_json []byte, object common.Object) error
	Get_object(ctx context.Context, get_json []byte, object common.Object) ([]byte, error)
	Modify_object(ctx context.Context, modify_json []byte, object common.Object) error
	Update_object(ctx context.Context, update_json []byte, object common.Object) error
}

type Cass struct {
}

func (u Cass) Modify_object(ctx context.Context, modify_json []byte, object common.Object) error {
	session := Cassandra.Session
	table := object.Get_table()
	otype := object.Get_type()
	stmt := "INSERT INTO "
	stmt += table
	stmt += " JSON ?"
	//stmt += " IF EXISTS"

	log.Println(stmt, string(modify_json))
	query := session.Query(stmt, modify_json).Consistency(gocql.One)

	if err := query.Exec(); err != nil {
		cerr := otype + err.Error()
		log.Println(cerr)
		err := errors.New(cerr)
		return err
	}

	return nil
}

func (u Cass) Update_object(ctx context.Context, update_json []byte, object common.Object) error {
	var obj common.Object_handler

	session := Cassandra.Session
	table := object.Get_table()
	otype := object.Get_type()
	stmt := "UPDATE " + table + " "
	//stmt += " IF EXISTS"

	uoptions := object.Get_uoptions()
	qoptions := object.Get_qoptions()
	uoptions.Where = qoptions.Where

	if len(update_json) > 0 {
		err := json.Unmarshal(update_json, &obj)
		if err == nil {
			tid := obj.Id
			name := obj.Name
			if name != "" {
				uoptions.Where["name"] = name
			} else if tid.Node() != nil {
				uoptions.Where["id"] = tid
			}
			//uoptions.Where["entity_id"] = entity_id
		} else {
			log.Println(err)
			return err
		}
	}

	Cassandra.Process_uoptions(&stmt, uoptions)

	log.Println(stmt, string(update_json))
	query := session.Query(stmt).Consistency(gocql.One)

	dest := make(map[string]interface{})
	if applied, err := query.MapScanCAS(dest); err != nil {
		cerr := otype + err.Error()
		log.Println(cerr)
		err := errors.New(cerr)
		return err
	} else {
		if !applied {
			log.Println("Entry missing :", dest)
			cerr := otype + " Entry does not exist. Update failed"
			err := errors.New(cerr)
			return err
		}
	}

	return nil
}

func (u Cass) Create_object(ctx context.Context, create_json []byte, object common.Object) error {
	session := Cassandra.Session
	table := object.Get_table()
	otype := object.Get_type()
	stmt := "INSERT INTO "
	stmt += table
	stmt += " JSON ?"
	stmt += " IF NOT EXISTS"
	log.Println(stmt, string(create_json))
	query := session.Query(stmt, create_json).Consistency(gocql.One)

	var dest map[string]interface{}

	dest = make(map[string]interface{})
	if applied, err := query.MapScanCAS(dest); err != nil {
		cerr := otype + err.Error()
		log.Println(cerr)
		err := errors.New(cerr)
		return err
	} else {
		if !applied {
			log.Println("Existing entry :", dest)
			cerr := otype + " Entry exists. Create failed"
			err := errors.New(cerr)
			return err
		}
	}

	/*
		if err := query.Exec(); err != nil {
			log.Println(err)
			return err
		}
	*/
	return nil
}

type myJSON struct {
	Array []byte
}

func (u Cass) List_paged_objects(ctx context.Context, list_json []byte, object common.Object, pstate interface{}) (response_json []byte, err error, pgstate interface{}) {

	session := Cassandra.Session
	//restrict_by_entity := ctx.Value("MyContextvalues").(common.ContextValues).Get(string(common.ContextEntityKey)).(gocql.UUID)

	table := object.Get_table()
	stmt := "SELECT JSON * FROM "
	stmt += table

	qoptions := object.Get_qoptions()

	Cassandra.Process_qoptions(&stmt, qoptions)

	log.Println(stmt)
	query := session.Query(stmt)
	query.PageSize(qoptions.Pagesize)
	query.Prefetch(5)

	if pstate == nil || pstate == "" {
		pstate = []byte{}
		query.PageState(pstate.([]byte))
	} else {
		query.PageState(pstate.([]byte))
	}

	iter := query.Consistency(gocql.One).Iter()

	rd, err := iter.SliceMap()
	if err != nil {
		iter.Close()
		log.Println(err)
		return []byte{}, err, nil
	}

	//fmt.Printf("RD ", rd)
	if len(rd) > 0 {
		var m []byte
		pgstate := iter.PageState()

		b := make([]byte, len(rd))
		m = []byte("[")
		for _, row := range rd {
			//println((row["[json]"].(string)))
			//b[i] += fmt.Sprintf("%s, ", (row["[json]"].(string)))
			b = []byte(fmt.Sprintf("%s,", row["[json]"].(string)))
			//		append(b, (row["[json]"].([]byte)))
			m = append(m, b...)
		}
		m = append(m[0:len(m)-1], "]"...)
		log.Println(string(m), "pagestate=", string(pgstate))
		iter.Close()
		return m, err, pgstate
	}
	return []byte{}, nil, nil
}

func (u Cass) List_objects(ctx context.Context, list_json []byte, object common.Object) (response_json []byte, err error) {
	var obj common.Object_handler

	session := Cassandra.Session
	//restrict_by_entity := ctx.Value("MyContextvalues").(common.ContextValues).Get(string(common.ContextEntityKey)).(gocql.UUID)

	table := object.Get_table()
	stmt := "SELECT JSON * FROM "
	stmt += table

	qoptions := object.Get_qoptions()
	serviced_entity_flag := object.Get_serviced_entity_flag()
	if serviced_entity_flag {
		qoptions.Where["serviced_by_entity_id"] = qoptions.Where["entity_id"]
		delete(qoptions.Where, "entity_id")
	}

	if len(list_json) > 0 {
		err := json.Unmarshal(list_json, &obj)
		if err == nil {
			tid := obj.Id
			name := obj.Name
			if len(qoptions.Where) == 0 {
				qoptions.Where = make(map[string]interface{}, 10)
			}
			if tid.Node() != nil {
				qoptions.Where["entity_id"] = tid
			} else if name != "" {
				qoptions.Where["name"] = name
			}
		} else {
			log.Println(err)
			return []byte{}, err
		}
	}

	Cassandra.Process_qoptions(&stmt, qoptions)

	log.Println(stmt)
	query := session.Query(stmt)

	if qoptions.Limit > 0 {
		query.PageSize(5 * qoptions.Limit)
	}

	iter := query.Consistency(gocql.One).Iter()

	rd, err := iter.SliceMap()
	if err != nil {
		iter.Close()
		log.Println(err)
		return []byte{}, err
	}

	//fmt.Printf("RD ", rd)
	if len(rd) > 0 {
		var m []byte
		b := make([]byte, len(rd))
		m = []byte("[")
		for _, row := range rd {
			//println((row["[json]"].(string)))
			//b[i] += fmt.Sprintf("%s, ", (row["[json]"].(string)))
			b = []byte(fmt.Sprintf("%s,", row["[json]"].(string)))
			//		append(b, (row["[json]"].([]byte)))
			m = append(m, b...)
		}
		m = append(m[0:len(m)-1], "]"...)
		log.Println(string(m))
		iter.Close()
		return m, err
	}
	return []byte{}, nil
}

func (u Cass) Delete_object(ctx context.Context, delete_json []byte, object common.Object) error {

	var obj common.Object_handler
	session := Cassandra.Session
	table := object.Get_table()

	err := json.Unmarshal(delete_json, &obj)
	if err != nil {
		log.Println(err)
		return err
	}

	tid := obj.Id
	name := obj.Name
	stmt := "DELETE FROM "
	stmt += table

	qoptions := object.Get_qoptions()
	if len(qoptions.Where) == 0 {
		qoptions.Where = make(map[string]interface{}, 10)
	}
	if name != "" {
		qoptions.Where["name"] = name
	} else if tid.Node() != nil {
		qoptions.Where["id"] = tid
	}

	Cassandra.Process_qoptions(&stmt, qoptions)
	//stmt += " where id = ?"
	log.Println(stmt)
	query := session.Query(stmt).Consistency(gocql.One)

	if err = query.Exec(); err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (u Cass) Get_object(ctx context.Context, get_json []byte, object common.Object) ([]byte, error) {

	var err error = nil
	var obj common.Object_handler

	session := Cassandra.Session
	table := object.Get_table()
	qoptions := object.Get_qoptions()

	if len(get_json) > 0 {
		err := json.Unmarshal(get_json, &obj)
		if err != nil {
			log.Println(err)
			return []byte{}, err
		}
		tid := obj.Id
		name := obj.Name
		if name != "" {
			qoptions.Where["name"] = name
		} else if tid.Node() != nil {
			qoptions.Where["id"] = tid
		}
	} else if len(qoptions.Where) == 0 {
		err = errors.New("Missing input")
		log.Println(err)
		return []byte{}, err
	}

	stmt := "SELECT JSON * FROM "
	stmt += table
	//	stmt += " where id = ?"

	//	qoptions.Where["entity_id"] = restrict_by_entity
	serviced_entity_flag := object.Get_serviced_entity_flag()

	if serviced_entity_flag {
		qoptions.Where["serviced_by_entity_id"] = qoptions.Where["entity_id"]
		delete(qoptions.Where, "entity_id")
	}
	Cassandra.Process_qoptions(&stmt, qoptions)

	log.Println(stmt)
	iter := session.Query(stmt).Consistency(gocql.One).Iter()
	m := make(map[string]interface{})

	if iter.MapScan(m) != true {
		err = errors.New("DB query Failed")
		iter.Close()
		return []byte{}, err
	} else {
		if err1 := iter.Close(); err1 != nil {
			log.Println(err1)
		}
	}
	if m == nil {
		err = errors.New("DB query Failed")
		return []byte{}, err
	}

	log.Println(string(m["[json]"].(string)))
	return []byte(m["[json]"].(string)), err
}
