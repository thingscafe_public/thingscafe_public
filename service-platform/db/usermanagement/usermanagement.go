//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package usermanagement

import (
	"fmt"
	"log"

	"github.com/gocql/gocql"
	"gitlab.com/service-platform/common"
	"gitlab.com/service-platform/db/Cassandra"
	"golang.org/x/crypto/bcrypt"
	//	uuid "github.com/satori/go.uuid"
)

type Cass struct {
}

var session1 gocql.Session

type User_info struct {
	Id                 gocql.UUID
	Email              string
	Ancestor_entity_id gocql.UUID
	Entity_id          gocql.UUID
	Role               common.User_role
	First_name         string
	Last_name          string
	kv                 map[string]interface{}
	tags               string
}

type User_cred struct {
	User_id gocql.UUID
	//Entity_id gocql.UUID
	Password string
}

func (u Cass) Create_user(user *User_info) (result bool) {
	session := Cassandra.Session
	query := session.Query("INSERT INTO analyzethings.user (id, entity_id, role, email, first_name, last_name) VALUES (?, ?, ?, ?, ?, ?)",
		gocql.TimeUUID(), user.Entity_id, user.Role, user.Email, user.First_name, user.Last_name)

	if err := query.Exec(); err != nil {
		log.Println(err)
		return false
	}
	return true
}

func (u Cass) List_users() []User_info {
	// list all users
	//u1 := uuid.Must(uuid.NewV4())
	var user User_info
	var users []User_info

	// users := make([]User_info, 1)
	// var user = new(User_info)
	// user_id, _ := uuid.NewV1()

	session := Cassandra.Session
	iter := session.Query(`SELECT id, entity_id, role, first_name, last_name, email FROM analyzethings.user_by_email`).Iter()
	for iter.Scan(&user.Id, &user.Entity_id, &user.Role, &user.First_name, &user.Last_name, &user.Email) {
		fmt.Println("User:", user.Id, user.Role, user.First_name, user.Last_name, user.Email)

		users = append(users, user)
	}
	if err := iter.Close(); err != nil {
		log.Println(err)
	}
	return users[:len(users)]
}

func (u Cass) Delete_user(id gocql.UUID) bool {
	session := Cassandra.Session

	query := session.Query(`DELETE FROM analyzethings.user WHERE id = ?`, id)

	if err := query.Exec(); err != nil {
		log.Println(err)
		return false
	}
	return true
}

func (u Cass) Get_user(email_addr string) (user_i *User_info, ok bool) {
	var user = new(User_info)
	//	user_id, _ := uuid.NewV1()
	user.Email = email_addr

	session := Cassandra.Session

	if err := session.Query("SELECT  id, ancestor_entity_id, entity_id, role, first_name, last_name FROM analyzethings.users WHERE email = ?",
		email_addr).Consistency(gocql.One).Scan(&user.Id, &user.Entity_id, &user.Role, &user.First_name, &user.Last_name); err != nil {
		log.Println(err)
		return user, false
	}
	fmt.Println("User:", user.Id, user.Role, user.First_name, user.Last_name)

	return user, true
}

func (u Cass) Get_user_credentials(id gocql.UUID) (user_i *User_cred, ok bool) {
	var user_cred = new(User_cred)
	//	user_id, _ := uuid.NewV1()

	session := Cassandra.Session

	if err := session.Query(`SELECT  id, password FROM analyzethings.user_credentials WHERE id = ?`,
		id).Consistency(gocql.One).Scan(&user_cred.User_id, &user_cred.Password); err != nil {
		log.Println(err)
		return user_cred, false
	}
	//	fmt.Println("user_cred.", user_cred.Id, user_cred.Role, user_cred.First_name, user_cred.Last_name)

	c := HashAndSalt([]byte("12333333Test"))
	log.Println(c)
	return user_cred, true
}

func (u Cass) Create_user_cred(user_cred *User_cred) (result bool) {
	session := Cassandra.Session
	hash := HashAndSalt([]byte(user_cred.Password))
	query := session.Query("INSERT INTO analyzethings.user_credentials (id, user_id, password, enabled ) VALUES (?, ?, ?, ?, ?)",
		gocql.TimeUUID(), user_cred.User_id, hash, true)

	if err := query.Exec(); err != nil {
		log.Println(err)
		return false
	}
	return true
}

func HashAndSalt(pwd []byte) string {

	// Use GenerateFromPassword to hash & salt pwd.
	// MinCost is just an integer constant provided by the bcrypt
	// package along with DefaultCost & MaxCost.
	// The cost can be any value you want provided it isn't lower
	// than the MinCost (4)
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	// GenerateFromPassword returns a byte slice so we need to
	// convert the bytes to a string and return it
	return string(hash)
}
