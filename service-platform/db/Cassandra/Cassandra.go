//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package Cassandra

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/gocql/gocql"
	"gitlab.com/service-platform/common"
)

var Session *gocql.Session

type Cass struct {
}

func Process_uoptions(stmt *string, uoptions *common.Update_options) {
	/*
			Set           map[string]interface{} `json:"set"`
			Where         map[string]interface{} `json:"where"`
			Contains_flag bool                   `json:"contains_flag"`
			In_flag       bool                   `json:"in_flag"`
			Ifexists_flag bool                   `json:"ifexists_flag"`
			Ttl           int                    `json:"ttl"`
			Timestamp     int                    `json:"timestamp"`
		    Operation     string                 `json:"operation"`
	*/
	if uoptions.Ttl > 0 {
		*stmt += fmt.Sprintf(" USING TTL %d,", uoptions.Ttl)
		if uoptions.Timestamp > 0 {
			*stmt += fmt.Sprintf("AND USING TIMESTAMP %d,", uoptions.Timestamp)
		}
	}

	if len(uoptions.Set) > 0 {
		*stmt += " SET"
		for k, v := range uoptions.Set {
			if strings.Contains(k, "id") || strings.Contains(k, "Id") {
				if uoptions.Operation == "add" {
					*stmt += fmt.Sprintf(" %s = %s + '%v' ,", k, k, v)
				} else if uoptions.Operation == "remove" {
					*stmt += fmt.Sprintf(" %s = %s - '%v' ,", k, k, v)
				} else {
					*stmt += fmt.Sprintf(" %s='%v' ,", k, v)
				}
			} else {
				var map_stmt string = ""
				log.Printf("Value type:%T ", v)

				vmap, mapok := v.(map[string]interface{})
				if mapok {
					// Iterate through the map
					map_stmt = "{"
					for k1, v1 := range vmap {
						vstr, ok := v1.(string)
						if uoptions.Operation == "remove" {
							map_stmt = fmt.Sprintf("%s,", k1)
						} else {
							if ok {
								map_stmt = fmt.Sprintf("%s:'%v',", k1, vstr)
							} else {
								map_stmt = fmt.Sprintf("%s:%v,", k1, v1)
							}
						}
					}
					final := len(map_stmt) - 1
					map_stmt = map_stmt[:final]
					map_stmt += "}"
					log.Println("Found map ", uoptions.Operation+":", map_stmt)
				}

				if uoptions.Operation == "add" {
					*stmt += fmt.Sprintf(" %s = %s + ", k, k)
				} else if uoptions.Operation == "remove" {
					*stmt += fmt.Sprintf(" %s = %s - ", k, k)
				} else {
					*stmt += fmt.Sprintf(" %s = ", k)
				}
				if !mapok {
					vstr, ok := v.(string)
					if ok {
						*stmt += fmt.Sprintf("'%s',", vstr)
					} else {
						*stmt += fmt.Sprintf("%v,", v)
					}
				} else {
					*stmt += map_stmt + ","
				}
			}
		}
		*stmt = (*stmt)[0 : len(*stmt)-1]
	}

	if len(uoptions.Where) > 0 {
		*stmt += " where"
		for k, v := range uoptions.Where {
			if uoptions.Contains_flag {
				*stmt += fmt.Sprintf(" %s CONTAINS %v AND", k, v)
				continue
			} else if uoptions.In_flag {
				*stmt += fmt.Sprintf(" %s IN %v AND", k, v)
				continue
			}
			if !strings.Contains(k, "id") && !strings.Contains(k, "Id") {
				*stmt += fmt.Sprintf(" %s='%v' AND", k, v)
			} else {
				*stmt += fmt.Sprintf(" %s=%v AND", k, v)
			}
		}
		*stmt = (*stmt)[0 : len(*stmt)-3]
	}

	// Only update IF row EXISTS
	*stmt += " IF EXISTS"
	/*
		if uoptions.Ifexists_flag {
			*stmt += "IF EXISTS"
		}
	*/
}

func Process_qoptions(stmt *string, qoptions *common.Query_options) {
	/*
		Where           map[string]interface{} `json:"where"`
		Groupby         string                 `json:"groupby"`
		Orderby         map[string]string      `json:"orderby"`
		Limit           int                    `json:"limit"`
		Allow_filtering bool                   `json:"allow_filtering"`
		Aggregate       map[string]interface{} `json:"aggregate"`
	*/
	if len(qoptions.Where) > 0 {
		*stmt += " WHERE"
		for k, v := range qoptions.Where {
			vstr, ok := v.(string)
			if ok {
				if strings.Contains(vstr, "key") || strings.Contains(vstr, "value") {
					if qoptions.Contains_flag {
						*stmt += fmt.Sprintf(" %s CONTAINS %v AND", k, v)
						continue
					}
				}
			}
			if !strings.Contains(k, "id") && !strings.Contains(k, "Id") {
				*stmt += fmt.Sprintf(" %s='%v' AND", k, v)
			} else {
				*stmt += fmt.Sprintf(" %s=%v AND", k, v)
			}
		}
		if len(qoptions.Where_complex) > 0 {
			for _, v := range qoptions.Where_complex {
				if v.Operator == "in" {
					var in_list string

					for _, rhs := range v.Rhs.([]string) {
						in_list += fmt.Sprintf("'%s', ", rhs)
					}

					l := len(in_list) - 2
					in_list = in_list[0:l]

					*stmt += fmt.Sprintf(" %s %s (%v) AND", v.Lhs, v.Operator, in_list)
				} else {
					*stmt += fmt.Sprintf(" %s %s '%v' AND", v.Lhs, v.Operator, v.Rhs)
				}
			}
		}
		*stmt = (*stmt)[0 : len(*stmt)-3]
		if len(qoptions.Orderby) > 0 {
			*stmt += " ORDER BY"
			for k, v := range qoptions.Orderby {
				*stmt += fmt.Sprintf(" %s %v", k, v)
			}
		}
	}

	if len(qoptions.Groupby) > 0 {
		*stmt += fmt.Sprintf(" GROUP BY %s ", qoptions.Groupby)
	}

	if qoptions.Limit > 0 {
		*stmt += fmt.Sprintf(" LIMIT %d ", qoptions.Limit)
	}

	if qoptions.Allow_filtering {
		*stmt += "ALLOW FILTERING"
	}
}

//func (c Cass) initialize() {
func Initialize() {
	// connect to the cluster
	var err error
	addr := os.Getenv("CASSANDRA")
	cluster := gocql.NewCluster(addr)

	/*
		 // Enable this code for production
			user := os.Getenv("CASSANDRA_USER")
			pass := os.Getenv("CASSANDRA_PASSWD")
			cluster.Authenticator = gocql.PasswordAuthenticator{
				Username: user,
				Password: pass,
			}
			cluster.RetryPolicy = &gocql.SimpleRetryPolicy{NumRetries: 1}
	*/

	cluster.Consistency = gocql.Quorum
	Session, err = cluster.CreateSession()
	if err != nil {
		panic(err)
	}
	fmt.Println("cassandra init done")
	//c.Session = Session
}

/*
func (c Cass) close() {
	// connect to the cluster
	c.Session.Close()
	fmt.Println("cassandra close done")
}
*/

func (c Cass) deferclose() {
	// connect to the cluster
	defer Session.Close()
}
