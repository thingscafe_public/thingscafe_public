
CREATE KEYSPACE saasv WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1'}  AND durable_writes = true;

CREATE TABLE saasv.config_profiles (
    name text,
    entity_id timeuuid,
    apply_time bigint,
    apply_to map<timeuuid, text>,
    aschema_id timeuuid,
    aschema_name text,
    config text,
    config_order int,
    cschema_id timeuuid,
    cschema_name text,
    id timeuuid,
    kv map<text, text>,
    profile_type text,
    profile_type_id timeuuid,
    profile_type_name text,
    published_to list<timeuuid>,
    sync_time bigint,
    PRIMARY KEY (name, entity_id)
) WITH CLUSTERING ORDER BY (entity_id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';
CREATE INDEX profile_apply_set ON saasv.config_profiles (keys(apply_to));
CREATE INDEX profile_published_to ON saasv.config_profiles (values(published_to));

CREATE MATERIALIZED VIEW saasv.config_profiles_by_entity AS
    SELECT *
    FROM saasv.config_profiles
    WHERE id IS NOT NULL AND name IS NOT NULL AND entity_id IS NOT NULL
    PRIMARY KEY (entity_id, name, id)
    WITH CLUSTERING ORDER BY (name ASC, id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.config_profile AS
    SELECT *
    FROM saasv.config_profiles
    WHERE id IS NOT NULL AND name IS NOT NULL AND entity_id IS NOT NULL
    PRIMARY KEY (id, name, entity_id)
    WITH CLUSTERING ORDER BY (name ASC, entity_id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.config_profiles_by_profile_type AS
    SELECT *
    FROM saasv.config_profiles
    WHERE profile_type_id IS NOT NULL AND entity_id IS NOT NULL AND name IS NOT NULL
    PRIMARY KEY (profile_type_id, name, entity_id)
    WITH CLUSTERING ORDER BY (name ASC, entity_id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE TABLE saasv.assets (
    name text,
    parent_asset_id timeuuid,
    entity_id timeuuid,
    asset_type timeuuid,
    collmap text,
    configured boolean,
    description text,
    id timeuuid,
    ipaddr inet,
    kv map<text, text>,
    location text,
    mobile text,
    root_asset_flag boolean,
    serial_no text,
    subtype text,
    tags text,
    user_defined boolean,
    PRIMARY KEY ((name, parent_asset_id), entity_id)
) WITH CLUSTERING ORDER BY (entity_id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.controlled_assets AS
    SELECT *
    FROM saasv.assets
    WHERE entity_id IS NOT NULL AND parent_asset_id IS NOT NULL AND name IS NOT NULL
    PRIMARY KEY (parent_asset_id, entity_id, name)
    WITH CLUSTERING ORDER BY (entity_id ASC, name ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.assets_by_entity AS
    SELECT *
    FROM saasv.assets
    WHERE entity_id IS NOT NULL AND id IS NOT NULL AND name IS NOT NULL AND parent_asset_id IS NOT NULL
    PRIMARY KEY (entity_id, parent_asset_id, name, id)
    WITH CLUSTERING ORDER BY (parent_asset_id ASC, name ASC, id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.asset AS
    SELECT *
    FROM saasv.assets
    WHERE entity_id IS NOT NULL AND name IS NOT NULL AND id IS NOT NULL AND parent_asset_id IS NOT NULL
    PRIMARY KEY (id, parent_asset_id, name, entity_id)
    WITH CLUSTERING ORDER BY (parent_asset_id ASC, name ASC, entity_id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.assets_by_asset_type AS
    SELECT *
    FROM saasv.assets
    WHERE asset_type IS NOT NULL AND entity_id IS NOT NULL AND name IS NOT NULL AND parent_asset_id IS NOT NULL
    PRIMARY KEY (asset_type, entity_id, parent_asset_id, name)
    WITH CLUSTERING ORDER BY (entity_id ASC, parent_asset_id ASC, name ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE TABLE saasv.user_credentials (
    id timeuuid PRIMARY KEY,
    kv map<text, text>,
    password text
) WITH bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE TABLE saasv.config_schemas (
    name text,
    id timeuuid,
    allowed_entity_type int,
    apply_schema_id timeuuid,
    apply_schema_name text,
    config_order int,
    cschema_json text,
    cschema_type text,
    cschema_type_id timeuuid,
    cschema_type_name text,
    description text,
    entity_id timeuuid,
    kv map<text, text>,
    PRIMARY KEY (name, id)
) WITH CLUSTERING ORDER BY (id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';
CREATE INDEX ordered_schemas ON saasv.config_schemas (config_order);

CREATE MATERIALIZED VIEW saasv.config_schema_by_schema_type AS
    SELECT *
    FROM saasv.config_schemas
    WHERE name IS NOT NULL AND id IS NOT NULL AND cschema_type_id IS NOT NULL
    PRIMARY KEY (cschema_type_id, name, id)
    WITH CLUSTERING ORDER BY (name ASC, id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE TABLE saasv.entities (
    name text,
    entity_id timeuuid,
    address text,
    address2 text,
    city text,
    country text,
    domain text,
    email text,
    entity_type int,
    id timeuuid,
    kv map<text, text>,
    phone text,
    phones list<text>,
    serviced_by_entity_id timeuuid,
    state text,
    tags text,
    zip text,
    PRIMARY KEY (name, entity_id)
) WITH CLUSTERING ORDER BY (entity_id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.entity AS
    SELECT *
    FROM saasv.entities
    WHERE id IS NOT NULL AND entity_id IS NOT NULL AND name IS NOT NULL
    PRIMARY KEY (id, name, entity_id)
    WITH CLUSTERING ORDER BY (name ASC, entity_id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.child_entities AS
    SELECT *
    FROM saasv.entities
    WHERE entity_id IS NOT NULL AND id IS NOT NULL AND name IS NOT NULL
    PRIMARY KEY (entity_id, name, id)
    WITH CLUSTERING ORDER BY (name ASC, id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.controlled_entities AS
    SELECT *
    FROM saasv.entities
    WHERE entity_id IS NOT NULL AND serviced_by_entity_id IS NOT NULL AND name IS NOT NULL
    PRIMARY KEY (serviced_by_entity_id, name, entity_id)
    WITH CLUSTERING ORDER BY (name ASC, entity_id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE TABLE saasv.asset_types (
    name text,
    id timeuuid,
    addressable boolean,
    collection_type_list map<timeuuid, text>,
    config text,
    controlled_by_asset_type_id timeuuid,
    correlation map<timeuuid, text>,
    description text,
    entity_id timeuuid,
    kind text,
    kv map<text, text>,
    root_asset_flag boolean,
    PRIMARY KEY (name, id)
) WITH CLUSTERING ORDER BY (id ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.custom_asset_types AS
    SELECT *
    FROM saasv.asset_types
    WHERE entity_id IS NOT NULL AND id IS NOT NULL AND name IS NOT NULL
    PRIMARY KEY (entity_id, id, name)
    WITH CLUSTERING ORDER BY (id ASC, name ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.asset_type AS
    SELECT *
    FROM saasv.asset_types
    WHERE id IS NOT NULL AND name IS NOT NULL
    PRIMARY KEY (id, name)
    WITH CLUSTERING ORDER BY (name ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.controlled_asset_types AS
    SELECT *
    FROM saasv.asset_types
    WHERE name IS NOT NULL AND id IS NOT NULL AND controlled_by_asset_type_id IS NOT NULL
    PRIMARY KEY (controlled_by_asset_type_id, id, name)
    WITH CLUSTERING ORDER BY (id ASC, name ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE TABLE saasv.users (
    email text,
    saas_entity_name text,
    entity_id timeuuid,
    first_name text,
    id timeuuid,
    kv map<text, text>,
    last_name text,
    role int,
    tags text,
    PRIMARY KEY (email, saas_entity_name)
) WITH CLUSTERING ORDER BY (saas_entity_name ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.user AS
    SELECT *
    FROM saasv.users
    WHERE email IS NOT NULL AND saas_entity_name IS NOT NULL AND id IS NOT NULL
    PRIMARY KEY (id, email, saas_entity_name)
    WITH CLUSTERING ORDER BY (email ASC, saas_entity_name ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW saasv.users_by_entity AS
    SELECT *
    FROM saasv.users
    WHERE email IS NOT NULL AND saas_entity_name IS NOT NULL AND entity_id IS NOT NULL
    PRIMARY KEY (entity_id, saas_entity_name, email)
    WITH CLUSTERING ORDER BY (saas_entity_name ASC, email ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';


CREATE KEYSPACE IF NOT EXISTS thingscafe_data WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1'}  AND durable_writes = true;

CREATE TYPE IF NOT EXISTS thingscafe_data.cntr_name (
    min bigint,
    max bigint,
    avg decimal,
    sum decimal,
    count bigint,
    rate decimal,
    stat_type text,
    abs bigint
);

CREATE FUNCTION IF NOT EXISTS thingscafe_data.state_group_and_count(state map<text, int>, type text)
    CALLED ON NULL INPUT
    RETURNS map<text, int>
    LANGUAGE java
    AS $$ Integer count = (Integer) state.get(type);  if (count == null) count = 1; else count++; state.put(type, count); return state; $$;

CREATE AGGREGATE IF NOT EXISTS thingscafe_data.group_and_count(text)
    SFUNC state_group_and_count
    STYPE map<text, int>
    INITCOND {};

CREATE TABLE thingscafe_data.saasv_events (
    entity_name text,
    severity text,
    ts timestamp,
    resource text,
    seq int,
    app text,
    asset_name text,
    asset_type text,
    day date,
    description text,
    details text,
    kv map<text, text>,
    PRIMARY KEY ((entity_name, severity), ts, resource, seq)
) WITH CLUSTERING ORDER BY (ts DESC, resource ASC, seq DESC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE MATERIALIZED VIEW thingscafe_data.saasv_events_by_asset AS
    SELECT *
    FROM thingscafe_data.saasv_events
    WHERE entity_name IS NOT NULL AND day IS NOT NULL AND ts IS NOT NULL AND resource IS NOT NULL AND seq IS NOT NULL AND severity IS NOT NULL
    PRIMARY KEY ((resource, severity), entity_name, ts, seq)
    WITH CLUSTERING ORDER BY (entity_name ASC, ts DESC, seq DESC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';

CREATE TABLE thingscafe_data.saasv_counters (
    resource_id uuid,
    entity_name text,
    aggregation_type text,
    asset_name text,
    ts timestamp,
    aggregation_interval int,
    aggregation_table map<text, frozen<cntr_name>>,
    kv map<text, text>,
    PRIMARY KEY ((resource_id, entity_name), aggregation_type, asset_name, ts)
) WITH CLUSTERING ORDER BY (aggregation_type ASC, asset_name ASC, ts DESC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = {'keys': 'ALL', 'rows_per_partition': 'NONE'}
    AND comment = ''
    AND compaction = {'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32', 'min_threshold': '4'}
    AND compression = {'chunk_length_in_kb': '64', 'class': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND crc_check_chance = 1.0
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99PERCENTILE';
CREATE INDEX IF NOT EXISTS thingscafe_counters_kv_table_idx ON thingscafe_data.saasv_counters (values(kv));

