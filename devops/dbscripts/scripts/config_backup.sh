# Backup config for a SAAS vendor after 
# making any changes
# Usage ./config_backup.sh <backup_dir> <saas vendor>
# Backing up important objects for a SAAS/IOT vendor
# Add to cron job if required 

mkdir -p $2
BACKUP_LIST="$1.config_schemas $1.asset_types $1.users $1.user_credentials $1.entities"
for i in $BACKUP_LIST
do
	echo "Backing up $i"
	bfile="$i.config-$(date +'%m%d%y')"
	echo  "select JSON * from $i;" | cqlsh `hostname -i` > $2/$bfile
done
