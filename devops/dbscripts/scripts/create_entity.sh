#!/bin/bash
# Use only when creating the SAAS entity
host=`hostname -i`
timeout=30
insert_cmd="insert into $1.entities (entity_id, id, name, domain, email, entity_type) VALUES ($2, now(), '$1', '$3', '$4', 1 ) IF NOT EXISTS;"
echo $insert_cmd | cqlsh --request-timeout=$timeout $host
#echo "Press Enter to continue"
#read a
copy_cmd="COPY $1.entities (id) to STDOUT;"
entity_id=`echo $copy_cmd | cqlsh --request-timeout=$timeout $host`
entity_id=`echo $entity_id`
#echo "Press Enter to continue"
#read a
create_user="insert into $1.users (email, entity_id, saas_entity_name, id, first_name, last_name, kv) VALUES ('$4', $entity_id, '$1', now(), '$5', '$6', {'entity_type': '1'}) IF NOT EXISTS;"
echo $create_user | cqlsh --request-timeout=$timeout $host
copy_cmd="COPY $1.users (id) to STDOUT;"
user_id=`echo $copy_cmd | cqlsh --request-timeout=$timeout $host`
echo $user_id
#echo "Press Enter to continue"
#read a
# Default password is test123. Change in app after login
passwd='$2a$04$o4CJ6dhUUKcka6Evp7fEnOsCLAvS49xCTbeWelpcVIkHsulJaxmw.'
cred_cmd="insert into $1.user_credentials (id, password) VALUES ($user_id, '$passwd');"
echo $cred_cmd | cqlsh --request-timeout=$timeout $host
echo $entity_id $user_id "test123"
