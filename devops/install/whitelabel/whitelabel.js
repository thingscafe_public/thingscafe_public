//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

const express = require ('express');
const app = express ();
app.set ('view engine', 'hbs');
app.use (express.static (__dirname + '/public'));

app.get ('/:orgName/manifest.json', (req, res) => {
  // You can dynamically generate your manifest here
  // You can pull the data from database and send it back
  // I will use a template for simplicity

  //Use some logic to extract organization name from referer
  //var matches = /\/([a-z]+)\/?$/i.exec (req.headers.referer);
  //if (matches && matches.length > 1) {
   // var orgName = matches[1];
  //} else {
   // var orgName = 'ORGA'; //Default
  //}

  // Need to set content type, default is text/html
  res.set ('Content-Type', 'application/json');
  var org = req.params.orgName;
  res.render ('manifest2.hbs', {orgName:org, Name: org.toUpperCase()});
});

app.get ('/:orgName', (req, res) => {
  res.render ('index2.hbs', {orgName: req.params.orgName});
});

app.listen (3000, () => console.log ('Whitelabel app listening on port 3000!'));
