# !/bin/bash

if [ "$#" -ne 1 ]
then
	echo "Usage : $0 <package>"
	exit -1
fi
echo "Installing $1"
set -x

cd /var/www/html
cd tcafe_staging
rm -rf tcafe.old
mv tcafe tcafe.old
tar -xvzf $1

service nginx restart
