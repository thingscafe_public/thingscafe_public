#!/bin/bash
env_file=./$1_env.sh
ports=$2
image=$3
CMD="docker run -i -t --env-file=$env_file \
        --name=$1 -h $1 \
        --mount type=bind,src=/home/tcafe/bin,dst=/home/tcafe/bin \
        --mount type=bind,src=/var/log/tcafe,dst=/var/log/tcafe \
        --cap-add=NET_ADMIN \
        -p $ports \
        --restart unless-stopped --privileged=true \
        --entrypoint "/usr/bin/supervisord" \
        $3 \
        -n -c /etc/supervisor/supervisord.conf > /var/log/tcafe_ops/tc_run.log 2>&1 "
echo "$CMD"
bash -c "$CMD"
