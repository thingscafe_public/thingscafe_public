
Login to get access token (change the host to the server ip. Replace localhost here with your own server)
Change password accordingly, also update the url to your own server where you have hosted the thingscafe 
service platform

curl -X POST \
   https://live.thingscafe.net/tc/login \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 96' \
  -H 'Content-Type: text/plain' \
  -H 'cache-control: no-cache' \
  -d '{ "username" : "admin@saasv.com", "password" : "*******", "saas_entity_name" : "saasv" }'

 Response would be similar to 

 {"id":"74a8b700-9f1e-11e9-a550-c7f00d9bf1ef","access_token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbWFpbCI6ImFkbWluQGF2YW5pamFsLmNvbSIsImVudGl0eSI6ImJlN2FmYWQwLTllZTktMTFlOS1hNTUwLWM3ZjAwZDliZjFlZiIsImVudGl0eV90eXBlIjoiLTEiLCJleHAiOjE1ODg3NDMyOTgsInNhYXNlbnRpdHkiOiJhdmFuaWphbCIsInVzZXIiOiI3NGE4YjcwMC05ZjFlLTExZTktYTU1MC1jN2YwMGQ5YmYxZWYiLCJ1c2VyX3JvbGUiOjJ9.p5wc8OFE2j-bE2n4gTnedmwHDwwZuk28tQ6XKnktsls","refresh_token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbWFpbCI6ImFkbWluQGF2YW5pamFsLmNvbSIsImVudGl0eSI6ImJlN2FmYWQwLTllZTktMTFlOS1hNTUwLWM3ZjAwZDliZjFlZiIsImVudGl0eV90eXBlIjoiLTEiLCJleHAiOjE1ODg4Mjk2OTgsInNhYXNlbnRpdHkiOiJhdmFuaWphbCIsInVzZXIiOiI3NGE4YjcwMC05ZjFlLTExZTktYTU1MC1jN2YwMGQ5YmYxZWYiLCJ1c2VyX3JvbGUiOjJ9.iB0OZkao8bbOtUio_tD6VXExoy_-PfY2zsx6NxlQ5nE","expires_in":172800,"saas_entity_name":""}

# Copy paste the access token into the Authorization header as shown below for all other calls.


# Get Asset types definition

curl -X GET \
  https://live.thingscafe.net/tc/v1/assets/list-asset-types \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbWFpbCI6ImFkbWluQGF2YW5pamFsc3AuY29tIiwiZW50aXR5IjoiYzZhZWFjYmMtYTE0MC0xMWU5LThjZDctMDI0MmFjMTEwMDA1IiwiZW50aXR5X3R5cGUiOiIyIiwiZXhwIjoxNTcyNDUxODkyLCJzYWFzZW50aXR5IjoiYXZhbmlqYWwiLCJ1c2VyIjoiY2E4MGI3ZGEtYTE0MC0xMWU5LThjZDgtMDI0MmFjMTEwMDA1IiwidXNlcl9yb2xlIjoyfQ.gCDP5HI_WrZX-XxDczi_dAHd9aTBxQQzD9sy8_gDues' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 109' \
  -H 'Content-Type: application/json' \
  -H 'Host: live.thingscafe.net' \
  -H 'cache-control: no-cache' \
  -d '
{
"debug": true,
"encode": "1c",
"site_name": "AJoffice",
"controller_name" : "C1",
"flags": "incremental"
}'


# Create new config schema type
curl -X POST \
  https://live.thingscafe.net/tc/v1/config/create-schema \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbWFpbCI6ImFkbWluQGF2YW5pamFsc3AuY29tIiwiZW50aXR5IjoiYzZhZWFjYmMtYTE0MC0xMWU5LThjZDctMDI0MmFjMTEwMDA1IiwiZXhwIjoxNTY1MzQyMDEwLCJzYWFzZW50aXR5IjoiYXZhbmlqYWwiLCJ1c2VyIjoiY2E4MGI3ZGEtYTE0MC0xMWU5LThjZDgtMDI0MmFjMTEwMDA1IiwidXNlcl9yb2xlIjoyfQ.ZZf5uu8r53tbPY-YVj83frQ5KUBuJbTJvZ4Xcf09Riw' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 393' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
    "name": "Program",
    "description": "Schema for Program Version 1",
    "entity_id": "be7afad0-9ee9-11e9-a550-c7f00d9bf1ef",
    "allowed_entity_type": 3,
    "apply_schema_id": "09c13a67-3801-11e9-8502-0242ac110004",
    "cschema_type": "owner_config",
    "cschema_type_id": "098994ff-9ff9-11e9-8cc7-0242ac110005",
    "cschema_type_name": "Circuit"
}'


# Update schema with config schema json. Use the id of the specific config schema type and 
# copy the config schema json into cschema_json field

curl -X POST \
  https://live.thingscafe.net/tc/v1/config/create-schema \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbWFpbCI6ImFkbWluQGF2YW5pamFsc3AuY29tIiwiZW50aXR5IjoiYzZhZWFjYmMtYTE0MC0xMWU5LThjZDctMDI0MmFjMTEwMDA1IiwiZXhwIjoxNTY1MzQyMDEwLCJzYWFzZW50aXR5IjoiYXZhbmlqYWwiLCJ1c2VyIjoiY2E4MGI3ZGEtYTE0MC0xMWU5LThjZDgtMDI0MmFjMTEwMDA1IiwidXNlcl9yb2xlIjoyfQ.ZZf5uu8r53tbPY-YVj83frQ5KUBuJbTJvZ4Xcf09Riw' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
        "id": "7b8f428a-4b9f-11e9-a278-0242ac110004",
        "entity_id": "d181da46-ffa1-11e8-8eb2-f2801f1b9fd1",
        "allowed_entity_type": 3,
        "apply_schema_id": "09c13a67-3801-11e9-8502-0242ac110004",
        "cschema_json": "[  \n    {  \n        \"type\": \"input\",  \n        \"label\": \"Schedule name\",  \n        \"name\": \"name\",  \n        \"placeholder\": \"Enter Schedule name\" ,\n\t\"valuetype\" : \"text\",\n\t\"minlength\" : \"2\",\n\t\"maxlength\" : \"6\",\n\t\"optional\" : false\n    },  \n    {  \n        \"type\": \"select\",  \n        \"label\": \"cycle\",  \n        \"name\": \"manner\",  \n        \"placeholder\": \"Select option\",\n\t\"optional\" : false,\n\t\"valuetype\" : \"select\",\n\t\"options\": [\n\t\t\"Continuous\",\n\t\t\"Scheduled\",\n\t\t\"Manual Stop\",\n\t\t\"Manual Start\"\n\t]\n    },  \n    {  \n        \"type\": \"cinput\",  \n        \"label\": \"Days\",  \n        \"name\": \"days\",  \n        \"placeholder\": \"Enter days\",\n\t\"valuetype\" : \"number\",\n\t\"min\" : \"0\",\n\t\"max\" : \"44\",\n\t\"optional\" : false,\n\t\"only_if_key\" : \"manner\",\n\t\"only_if_value\" : \"Scheduled\"\n    },  \n    {  \n        \"type\": \"cinput\",  \n        \"label\": \"Hours\",  \n        \"name\": \"hours\",  \n        \"placeholder\": \"Enter hours\",\n\t\"valuetype\" : \"number\",\n\t\"min\" : \"0\",\n\t\"max\" : \"23\",\n\t\"optional\" : false,\n\t\"only_if_key\" : \"manner\",\n\t\"only_if_value\" : \"Scheduled\"\n    },  \n    {  \n        \"type\": \"cinput\",  \n        \"label\": \"Minutes(Max 59)\",  \n        \"name\": \"minutes\",  \n        \"placeholder\": \"Enter minutes\",\n\t\"valuetype\" : \"number\",\n\t\"min\" : \"0\",\n\t\"max\" : \"59\",\n\t\"optional\" : false,\n\t\"only_if_key\" : \"manner\",\n\t\"only_if_value\" : \"Scheduled\"\n    },  \n    {  \n        \"label\": \"Submit\",  \n        \"name\": \"submit\",  \n        \"type\": \"button\",  \n        \"function\": \"save()\",\n        \"placeholder\": \"\",\n\t\"optional\" : true\n    }  \n]",
        "cschema_type": "owner_config",
        "cschema_type_id": "c7377400-2847-11e9-92b1-97234f04ea82",
        "cschema_type_name": "Circuit",
        "description": "Schema for Schedule 1",
        "kv": null,
        "name": "Schedule"
    }'


# List all config schemas (check result in config.schemas/list_config_schemas.json file)

curl -X GET \
  https://live.thingscafe.net/tc/v1/config/list-schemas \
  -H 'Accept: */*' \
    -H 'Accept-Encoding: gzip, deflate' \
	  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbWFpbCI6ImFkbWluQGF2YW5pamFsLmNvbSIsImVudGl0eSI6ImJlN2FmYWQwLTllZTktMTFlOS1hNTUwLWM3ZjAwZDliZjFlZiIsImVudGl0eV90eXBlIjoiLTEiLCJleHAiOjE1ODg3NDMyOTgsInNhYXNlbnRpdHkiOiJhdmFuaWphbCIsInVzZXIiOiI3NGE4YjcwMC05ZjFlLTExZTktYTU1MC1jN2YwMGQ5YmYxZWYiLCJ1c2VyX3JvbGUiOjJ9.p5wc8OFE2j-bE2n4gTnedmwHDwwZuk28tQ6XKnktsls' \
	    -H 'Cache-Control: no-cache' \
		-H 'Content-Type: application/json' \
		-H 'Host: live.thingscafe.net' \
		-H 'cache-control: no-cache'
