//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { AuthGuard } from './guards/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//import { EntityPageModule } from './home/entity/entity.module';
//import { EntityPage } from './home/entity/entity.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
	loadChildren: './home/home.module#HomePageModule',
	canActivate: [AuthGuard]
  },
  {
    path: 'settings', 
    loadChildren: './settings/settings.module#SettingsPageModule' 
  },
  { 
	path: 'entity', 
	loadChildren: './home/entity/entity.module#EntityPageModule',
	canActivate: [AuthGuard]
  },
  { 
	path: 'config', 
	loadChildren: './home/config/config.module#ConfigPageModule', 
	canActivate: [AuthGuard]
  }
	/*
  { path: 'operations', loadChildren: './home/operations/operations.module#OperationsPageModule' }
  { path: 'schema', loadChildren: './home/config/schema/schema.module#SchemaPageModule' }
  { path: 'asset', loadChildren: './home/asset/asset.module#AssetPageModule' },
  { path: 'asset-fill', loadChildren: './home/asset/asset-fill/asset-fill.module#AssetFillPageModule' }
	{ path: 'home/entity', 
	loadChildren: './home/entity/entity.module#EntityPageModule' 
   	},
	{ path: 'home/entity/:id', loadChildren: './home/entity/entity.module#EntityPageModule' },
	 */
	//{ path: 'entity-fill', loadChildren: './home/entity/entity-fill/entity-fill.module#EntityFillPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
