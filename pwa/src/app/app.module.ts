//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from "@angular/common";
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CacheModule } from 'ionic-cache-observable';
import { ReactiveFormsModule } from '@angular/forms';
import {APP_BASE_HREF} from '@angular/common';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { InterceptorService } from './interceptors/interceptor.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from  '@angular/common/http';

import { SharedServicesModule } from  './services/sharedservices.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { MenuComponent } from './menu/menu.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


@NgModule({
  declarations: [AppComponent,  MenuComponent],
  entryComponents: [],
  exports: [MenuComponent],
  imports: [
    BrowserModule,
	CacheModule,
    IonicModule.forRoot(),
	IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CommonModule,
    SharedServicesModule.forRoot(),
    ReactiveFormsModule,
	NgxDatatableModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    StatusBar,
    SplashScreen,
	  //{provide: APP_BASE_HREF, useValue: '/rkauto'},
	{provide: APP_BASE_HREF, useValue: window['base-href']},
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi:true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
