import { TestBed } from '@angular/core/testing';

import { AssetTypeService } from './asset-type.service';

describe('AssetTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AssetTypeService = TestBed.get(AssetTypeService);
    expect(service).toBeTruthy();
  });
});
