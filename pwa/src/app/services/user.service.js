"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var rxjs_1 = require("rxjs");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
var TOKEN_KEY = "X-Auth-Token";
var USER_NAME = "USER_NAME";
var user_auth = /** @class */ (function () {
    function user_auth(values) {
        if (values === void 0) { values = {}; }
        Object.assign(this, values);
    }
    return user_auth;
}());
exports.user_auth = user_auth;
var user_info = /** @class */ (function () {
    function user_info(values) {
        if (values === void 0) { values = {}; }
        Object.assign(this, values);
    }
    user_info.prototype.get_name = function () {
        return (this.first_name + ' ' + this.last_name);
    };
    return user_info;
}());
exports.user_info = user_info;
var resturl = "/tc/v1/usersv2/";
var UserService = /** @class */ (function () {
    function UserService(httpClient, platform, storage) {
        var _this = this;
        this.httpClient = httpClient;
        this.platform = platform;
        this.storage = storage;
        this.hostUrl = "http:/localhost:8081";
        this.authState$ = new rxjs_1.BehaviorSubject(null);
        this.platform.ready().then(function () {
            _this.checkToken();
        });
    }
    UserService.prototype.checkToken = function () {
        var _this = this;
        this.storage.get(TOKEN_KEY).then(function (res) {
            if (res) {
                _this.authState$.next(true);
            }
            else {
                _this.authState$.next(false);
            }
        });
    };
    UserService.prototype.seturl = function (s) {
        this.hostUrl = "http://" + s.server;
        console.log("URL set to", this.hostUrl);
    };
    UserService.prototype.checkloginstatusObserver = function () {
        return this.authState$.asObservable();
    };
    UserService.prototype.checkloginstatus = function () {
        return this.authState$.value;
    };
    UserService.prototype.setloginstatus = function (s) {
        this.authState$.next(s);
        console.log("Logged in status set to", s);
    };
    UserService.prototype.get_self = function () {
        var url = this.hostUrl + resturl + "get-user";
        var hdr = new http_1.HttpHeaders();
        console.log("Contacting", url);
        hdr = hdr.append('Accept', 'application/json');
        hdr = hdr.append("Authorization", "Bearer " + this.auth.access_token);
        return this.httpClient
            .get(url, { headers: hdr })
            .map(function (response) {
            return new user_info(response);
        });
        //.catch((error)=>{
        //	console.error(error);
        //});
    };
    UserService.prototype.store_user_auth = function (auth) {
        var _this = this;
        this.auth = new user_auth(auth);
        this.storage.set(TOKEN_KEY, this.auth.access_token).then(function (res) {
            // TBD User FLATMAP here
            _this.authState$.next(true);
            console.log("Logged in");
            _this.get_self().subscribe(function (response) {
                var obj = new user_info(response);
                // get_self returns an array
                _this.user = obj[0];
                _this.storage.set(USER_NAME, (_this.user.first_name + ' ' + _this.user.last_name)).then(function (res) {
                    if (res) {
                        console.log("Stored user name ", res);
                    }
                });
            });
        });
    };
    UserService.prototype.get_user_name = function () {
        return this.storage.get(USER_NAME).then(function (res) {
            return (res);
        });
    };
    UserService.prototype.delete_user_auth = function () {
        this.authState$.next(false);
        this.storage.remove(TOKEN_KEY).then(function (res) {
            console.log("Logged out");
        });
    };
    UserService = __decorate([
        core_1.Injectable()
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
