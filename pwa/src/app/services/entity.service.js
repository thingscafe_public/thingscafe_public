"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
//import  'rxjs/add/operator/map';
var TOKEN_KEY = "X-Auth-Token";
var USER_NAME = "USER_NAME";
var HOST_URL = "HOST_URL";
var ENTITY_ID = "ENTITY_ID";
var entity_info = /** @class */ (function () {
    function entity_info() {
    }
    return entity_info;
}());
exports.entity_info = entity_info;
var entity_inf = /** @class */ (function () {
    function entity_inf(values) {
        if (values === void 0) { values = {}; }
        Object.assign(this.entity_i, values);
    }
    entity_inf.prototype.get_entity_id = function () {
        return (this.entity_i.id);
    };
    return entity_inf;
}());
exports.entity_inf = entity_inf;
var resturl = "/tc/v1/entitiesv2/";
var EntityService = /** @class */ (function () {
    function EntityService(httpClient, platform, storage) {
        var _this = this;
        this.httpClient = httpClient;
        this.platform = platform;
        this.storage = storage;
        this.hostUrl = "http://localhost:8081";
        var getpromises = [];
        this.settingsloaded = false;
        this.platform.ready().then(function () {
            //
            getpromises.push(_this.get_stored_value(HOST_URL)
                .then(function (resp) { _this.hostUrl = resp; }));
            //			this.hostUrl = this.get_stored_value(HOST_URL)
            getpromises.push(_this.get_stored_value(TOKEN_KEY)
                .then(function (resp) { _this.token = resp; }));
            getpromises.push(_this.get_stored_value(ENTITY_ID)
                .then(function (resp) { _this.user_entity = resp; }));
            Promise.all(getpromises).then(function (data) {
                console.log("Entity Service : Settings loaded");
                _this.settingsloaded = true;
            });
        });
    }
    EntityService.prototype.get_self = function () {
        var url = this.hostUrl + resturl + "get-entity";
        var hdr = new http_1.HttpHeaders();
        //let token = this.get_stored_value(TOKEN_KEY);
        console.log("Contacting", url);
        hdr = hdr.append('Accept', 'application/json');
        //hdr = hdr.append("Authorization", "Bearer " + this.token) ;
        return this.httpClient
            .get(url, { headers: hdr })
            .map(function (response) {
            return new entity_inf(response);
        });
    };
    EntityService.prototype.get_entities = function (entity_id, which) {
        if (!this.settingsloaded) {
            console.log("Entity service not ready");
        }
        var url = this.hostUrl + resturl + "list-controlled-entities";
        if (which == "owned") {
            url = this.hostUrl + resturl + "list-child-entities";
        }
        var hdr = new http_1.HttpHeaders();
        var input;
        input.id = entity_id;
        var input_json = JSON.stringify(input);
        console.log("Contacting", url);
        hdr = hdr.append('Accept', 'application/json');
        //hdr = hdr.append("Authorization", "Bearer " + this.token) ;
        return this.httpClient
            .post(url, input_json, { headers: hdr })
            .pipe(operators_1.retry(3), (operators_1.map(function (response) { console.log(response); return response; })), operators_1.catchError(this.handleError));
    };
    EntityService.prototype.get_child_entities = function () {
        if (!this.settingsloaded) {
            console.log("Entity service not ready");
        }
        var url = this.hostUrl + resturl;
        var hdr = new http_1.HttpHeaders();
        console.log("Contacting", url);
        hdr = hdr.append('Accept', 'application/json');
        //hdr = hdr.append("Authorization", "Bearer " + this.token) ;
        return this.httpClient
            .get(url, { headers: hdr })
            .pipe(operators_1.retry(3), (operators_1.map(function (response) { console.log(response); return response; })), operators_1.catchError(this.handleError));
    };
    EntityService.prototype.get_stored = function (key) {
        return this.storage.get(key).then(function (res) {
            if (res) {
                console.log('Read from storage ', key, res);
                return (res);
            }
        });
    };
    EntityService.prototype.get_stored_value = function (key) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.get(key)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    EntityService.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error("Backend returned code " + error.status + ", " +
                ("body was: " + error.error));
        }
        // return an observable with a user-facing error message
        return rxjs_1.throwError('Something bad happened; please try again later.');
    };
    EntityService = __decorate([
        core_1.Injectable()
    ], EntityService);
    return EntityService;
}());
exports.EntityService = EntityService;
