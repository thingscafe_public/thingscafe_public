"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
//import 'rxjs/add/observable/throw';
var EntityPage = /** @class */ (function () {
    function EntityPage(entitysvc, cachesvc) {
        this.entitysvc = entitysvc;
        this.cachesvc = cachesvc;
        this.keys = ["name", "entity_type_str", "phone"];
        this.current_entity = this.entitysvc.user_entity;
        this.view = "controlled";
    }
    EntityPage.prototype.ionViewWillEnter = function () {
        this.load_entity_page();
    };
    EntityPage.prototype.ngOnInit = function () {
        this.load_entity_page();
    };
    EntityPage.prototype.load_entity_page = function (id, view) {
        var _this = this;
        if (id == null)
            id = this.current_entity;
        if (view == null)
            view = this.view;
        var dataObservable = this.entitysvc.get_entities(id, view)
            .map(function (response) {
            return response.map(function (item) {
                var entity_type_str;
                switch (item.entity_type) {
                    case 2:
                        entity_type_str = "SERVICE_PROVIDER";
                        break;
                    case 1:
                        entity_type_str = "SAAS PROVIDER";
                        break;
                    case 4:
                        entity_type_str = "SITE";
                        break;
                    case 3:
                        entity_type_str = "CONSUMER";
                        break;
                    default:
                        entity_type_str = "MISCELLANEOUS";
                        break;
                }
                return (__assign({}, item, { "entity_type_str": entity_type_str }));
            });
        });
        this.cachesvc.register('home', dataObservable)
            .subscribe(function (cache) {
            _this.cache = cache;
            _this.data$ = _this.cache.get$;
            // Refresh the cache immediately.
            _this.cache.refresh().subscribe();
        });
    };
    EntityPage.prototype.onRefresh = function (refresher) {
        // Check if the cache has registered.
        if (this.cache) {
            this.cache.refresh().subscribe(function () {
                // Refresh completed, complete the refresher animation.
                refresher.complete();
            }, function (err) {
                // Something went wrong!
                // Log the error and cancel refresher animation.
                console.error('Refresh failed!', err);
                refresher.cancel();
            });
        }
        else {
            // Cache is not registered yet, so cancel refresher animation.
            refresher.cancel();
        }
    };
    EntityPage.prototype.load_linked_entity_page = function (id) {
        load_entity_page(id, "owned");
    };
    EntityPage = __decorate([
        core_1.Component({
            selector: 'app-entity',
            templateUrl: './entity.page.html',
            styleUrls: ['./entity.page.scss']
        })
    ], EntityPage);
    return EntityPage;
}());
exports.EntityPage = EntityPage;
