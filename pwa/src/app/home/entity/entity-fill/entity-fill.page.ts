//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { Input, Component, OnInit } from '@angular/core';
import { AbstractControl, ReactiveFormsModule, FormArray, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NavParams, NavController , ModalController} from '@ionic/angular';
import {entity_info} from '../../../services/entity.service';
import {CustomValidator} from '../../../common/custom-validator';

@Component({
  selector: 'app-entity-fill',
  templateUrl: './entity-fill.page.html',
  styleUrls: ['./entity-fill.page.scss'],
})

export class EntityFillPage implements OnInit {

	@Input() entity:entity_info;
	@Input() etype_str:string;
	@Input() action:string;
	@Input() parent_entity:entity_info;
	@Input() entities:entity_info[];

	config:any[]; 
	changeForm: FormGroup;

	submitAttempt: boolean = false;
	title:string;

	/*
	validation_messages = {
		'name': [
			{ type: 'required', message: 'Input is required' },
			{ type: 'minlength', message: 'Text length too small' },
			{ type: 'maxlength', message: 'Text length exceeds maximum ' },
		],
		'select': [
			{ type: 'required', message: 'Please select an option' },
		],
		'email': [
			{ type: 'required', message: 'Email is required' },
			{ type: 'email', message: 'Enter a valid email' }
		],
		'phones': [
			{ type: 'required', message: 'Phone is required' },
			{ type: 'validCountryPhone', message: 'Enter a valid phone number' }
		],
		'domain': [
			{ type: 'required', message: 'Input is required' },
			{ type: 'domain', message: 'Domain should be of form xxx.yyy' }
		],
		'password': [
			{ type: 'required', message: 'Password is required' },
			{ type: 'minlength', message: 'Password must be at least 5 characters long' },
			{ type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number' }
		]
	}
	 */

	ngOnInit() {
		//		console.log("Config :",this.config);
		if (this.action == 'Add') {
			this.entity = new entity_info(this.parent_entity);
			for (let key of Object.keys(this.entity)) {
				this.entity[key] = '';
			}
			if (this.parent_entity.entity_type == 3) {
				this.entity.serviced_by_entity_id = this.parent_entity.entity_id;
			} else {
				this.entity.serviced_by_entity_id = this.parent_entity.id;
			}
			this.entity.entity_id = this.parent_entity.id;
			this.entity.entity_type = this.parent_entity.entity_type+1;
		} else if (this.action == 'Copy') {
			let entity = new entity_info(this.entity);
			this.entity = entity;
		}
		console.log("Entity type ", this.etype_str, ":", this.action);
		this.title = this.etype_str;
	    this.changeForm = this.creategroup();
		  /*
		this.slideOneForm = this.formBuilder.group({
		  firstname: ['', Validators.compose([Validators.required, Validators.maxLength(30), Validators.pattern('[a-zA-Z]')])],
		  lastname: ['', Validators.required],
		  age: ['', Validators.required]
		});
		   */
	}

	ionViewWillEnter(){
	}

	private get_entity(name:string) {
		if (this.entities) {
			for (let entity of this.entities) {
				if (entity.name != name) 
					continue;
				return entity;
			}
		}
		return null;
	}

	toArray(obj){ 
		//return Object.keys(obj).map(key => obj[key]) 
		return Object.keys(obj);
	}

	private creategroup():FormGroup {

		const group = this.formBuilder.group({
		},{ updateOn: 'blur' });
		
		var value:any;
		var disabled:boolean;
		for (let field of Object.keys(this.entity)) {
			disabled = false;
			value = this.entity[field];
			if (this.action == "Modify" || this.action == "Copy") {
				if (this.action == "Copy" && 
					((field == "name") || (field == "phones") || (field == "email"))) {
					value = '';
				} else if (this.action == "Modify" && 
					((field == "name") || (field == "email"))) {
					disabled = true;
					if (field == "name") {
						this.title = this.etype_str + " " + value;
					}
				} 
			}
			if (field == 'phones' ) {
			  if (this.entity.entity_type == 3) {
				  let validation_list = this.customvalidator.create_validations(this.entity.entity_type, "phones");
				  group.setControl('phones', new FormArray([]));
				  let phones = group.get('phones') as FormArray;
				  if (this.action === "Add") {
					  phones.push(this.formBuilder.control({value:'', disabled:false}, Validators.compose(validation_list)));
						//validation_list.pop();
					  phones.push(this.formBuilder.control({value:'', disabled:false}, Validators.compose(validation_list)));
					  phones.push(this.formBuilder.control({value:'', disabled:false}, Validators.compose(validation_list)));
				  } else {
					  phones.push(this.formBuilder.control({value:value[0], disabled:false}, Validators.compose(validation_list)));
						//validation_list.pop();
					  phones.push(this.formBuilder.control({value:value[1], disabled:false}, Validators.compose(validation_list)));
					  phones.push(this.formBuilder.control({value:value[2], disabled:false}, Validators.compose(validation_list)));
				  }

				  validation_list = this.customvalidator.create_validations(this.entity.entity_type, "requiredList");
				  phones.setValidators(Validators.compose(validation_list));
					//group.addControl(field, this.formBuilder.control("", Validators.compose(validation_list)));
				}
			} else {
				let validation_list = this.customvalidator.create_validations(this.entity.entity_type, field);
				group.addControl(field, this.formBuilder.control({value:value, disabled:disabled}, Validators.compose(validation_list)));
			}
			//group.addControl(field, this.formBuilder.control(''));
		}
		
		if (this.action == 'Add' || this.action == 'Copy') {
			group.removeControl('id');
		}

		if (this.entity.entity_type <=3) {
			group.removeControl('address');
			group.removeControl('address2');
		} else {
			group.removeControl('phones');
			group.removeControl('domain');
			group.removeControl('email');
		}
		group.removeControl('kv');
		group.removeControl('type');
		group.removeControl('search_text');
		group.removeControl('roles');


		/*
		if (this.action == "Add") {
			let asset_map = this.atypinfo.collection_type_list;
			if (asset_map) {
				group.setControl('collmap', new FormArray([]));
				let collections = group.get('collmap') as FormArray;
				Object.keys(asset_map).map(key => {
					let atype_info = this.asset_type_details(key);
					//			let asset_choices = this.get_asset_options(key, asset_map[key]);
					let asset_choices = this.get_asset_choices(key);
					collections.push(this.formBuilder.group({
						asset_type: key,
						asset_type_name: atype_info.name,
						chosen_list : '',
						alist : [asset_choices, Validators.required],
						subtype_list : [this.subtypes, Validators.required],
						asset_id :'',
						properties : ''
					}));
				});
			}
		}
		*/

		return group;
	}

	private getControls() {
	  return(<FormArray> (this.changeForm.get('collmap')));
	}

	private getvalue(i:any) {
	  let control = <FormArray>(this.getControls());
	  let g = control.controls[i]['controls'];
	  return (g['alist'].value);
	}

	private getname(i:any) {
	  let control = <FormArray>(this.getControls());
	  let g = control.controls[i]['controls'];
	  return (g['asset_type_name'].value);
	}


	constructor(public navCtrl: NavController, 
		public formBuilder: FormBuilder,
		public customvalidator: CustomValidator,
		private modalCtrl: ModalController
	) { }

	save() {
		console.log("Saved");
		console.log(this.changeForm.value);
		this.modalCtrl.dismiss(this.changeForm.getRawValue());
	}

	cancel() {
	  console.log("Cancelled");
	  this.modalCtrl.dismiss(null);
	}

}
