//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { ViewEncapsulation, NgModule, Input, Component, OnInit , ViewChild, ElementRef} from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { take, map, filter } from 'rxjs/operators';
import { CommonModule } from "@angular/common";
import { IonicModule } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Observable } from  'rxjs/Rx';

interface Config {
	records : string;
}

@Component({
  selector: 'app-showgrid',
  templateUrl: './showgrid.component.html',
  styleUrls: ['./showgrid.component.scss'],
  encapsulation: ViewEncapsulation.None
})



export class ShowgridComponent implements OnInit {
  @Input() input_data:any;
  @Input() getmore_cb: (d:any, x:any, count:number, period:string) => Observable<any>;
  @Input() cb_arg:any;
  @Input() title:string;
  @ViewChild('myTable') table: any;

  public config : Config;
  public columns : any;
  public rows : any = [];
  expanded: any = {};   
  data:any = {};
  backuprows = [];
  period:string = "today";

  readonly headerHeight = 50;
  readonly rowHeight = 30;
  readonly pageLimit = 10;
  isLoading: boolean;
  enableSummary : boolean = true;
  summaryPosition : string = "top";
  totalCount:number = 100;
  offset:number=0;

		  
  constructor(private modalCtrl: ModalController, 
			  private el: ElementRef,
	          private global: GlobalService ) { }

  ionViewOnEnter() {
	this.ngOnInit();
  }

  ngOnInit() {
		this.global.ready()
	      .pipe(take(1),)
		  .subscribe(resp => {

	     this.setup_grid(this.input_data)
		});
  }

  cache: any = {};

  setPage(pageInfo?) {

	var pageNumber = 0;
	var size = 10;
	
	if (pageInfo) {
		pageNumber = pageInfo.offset;
		size = pageInfo.pageSize;
		console.log(pageInfo);
	} 

	let start = (1 + pageNumber) * size;
	let fetch_threshold = size*2;

	if (this.isLoading) {
		return;
	}

	if (this.rows.length > 0 &&  (start + size) < this.rows.length ) {
	  return;
	}

	if (this.totalCount > 0 && start > this.totalCount ) {
	  return;
	}

	this.isLoading = true;

	this.getmore_cb(this.data, this.cb_arg, size, this.period)
      .pipe(take(1),)
	  .subscribe(resp1 => {
	   
	   this.data = resp1;
	   let resp = resp1.records.map(rec => {
		 let local_time = new Date(rec.ts);
		 return ({...rec, local_time : local_time.toLocaleString()});
	   });


	   let start1 = this.rows.length;
	   const rows = [...this.rows];
	// insert rows into new position
	   rows.splice(start1, 0, ...resp);
	   this.cache[pageNumber] = rows
	   this.backuprows = rows
		  //this.rows = rows
	   this.rows = [...rows]
	   this.isLoading = false;
	   if (resp1.count < size) {
		  this.totalCount = resp1.count;
	  } else {
		if (start1 + fetch_threshold >= this.totalCount) {
		  this.totalCount += fetch_threshold;
		}
	  }
	},
	e => {
	  console.log("Error in getting more entries");
	  this.totalCount = 0;
	  this.isLoading = false;
		//this.rows = []
	  this.data = null
	});
  }

  onPage(event?) {
	
	let ret = this.getmore_cb(this.data, this.cb_arg, 10, this.period)
	this.isLoading = true;
	  ret.subscribe(resp => {
		 this.data = resp;
		 this.rows = resp.records;
	  });
  }

   onScroll(offsetY: number) {
    // total height of all rows in the viewport
    const viewHeight = this.el.nativeElement.getBoundingClientRect().height - this.headerHeight;

    // check if we scrolled to the end of the viewport
    if (!this.isLoading && offsetY + viewHeight >= this.rows.length * this.rowHeight) {
      // total number of results to load
      let limit = this.pageLimit;

      // check if we haven't fetched any results yet
      if (this.rows.length === 0) {
        // calculate the number of rows that fit within viewport
        const pageSize = Math.ceil(viewHeight / this.rowHeight);

        // change the limit to pageSize such that we fill the first page entirely
        // (otherwise, we won't be able to scroll past it)
        limit = Math.max(pageSize, this.pageLimit);
      }
      this.onPage();
    }
  }

  setup_grid(d:any) {
	  
	if (this.title == "alarms") {
		this.columns = [
			{ prop : 'asset_name', name : 'Source', resizeable : true, flexGrow : 1 },
		  { prop: 'severity', name: 'Severity' , flexGrow : 1, resizeable : true },
			{ prop : 'description', name:'Alarm', resizeable : true,  flexGrow : 3},
			{ prop: 'local_time', name: 'At' , flexGrow : 2, resizeable : true,summaryFunc: cells => `${cells.length} total`}
		  ]; 
	} else {
		this.columns = [
			{ prop : 'asset_name', name : 'Asset', summaryFunc: cells => `${cells.length} total`, resizeable : true, flexGrow : 1 },
		  //{ prop: 'ts', name: 'Time' , flexGrow : 4, resizeable : true, width:120},
			{ prop : 'description', name:'Event', resizeable : true,  flexGrow : 3},
			{ prop: 'local_time', name: 'At' , flexGrow : 2, resizeable : true}
		  ]; 
	}
	this.rows = []
	/*
	 this.data = this.input_data;
	 this.rows = this.data.records;
	 this.cache[0] = this.data;
	 this.backuprows = this.rows
	 */
  }

  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }



  updateFilter(event) {
    const val = event.target.value.toLowerCase();

	if (val == "") {
	  this.rows = this.backuprows;
	  this.offset = 0;
	  this.isLoading = false;
	  return
	}  
    // filter our data
    let temp = this.backuprows.filter(function(d) {
	  if (d.asset_name) {
		  return d.asset_name.toLowerCase().indexOf(val) !== -1 || !val;
	  }
    });

    // update the rows
    this.rows = temp;
	this.isLoading = true;
  }

  periodChange(event) {
	  this.period = event.target.value;
	  console.log("Selected :", this.period);
	  this.refresh();
  }

  refresh() {
	  this.table.offset = 0;
	  this.offset = 0;
	  this.data = null;
	  this.rows = []
	  this.setPage();
  }

  close() {
	this.modalCtrl.dismiss(null);
  }
}


@NgModule({
   imports: [IonicModule,CommonModule, NgxDatatableModule],
   exports: [ShowgridComponent],
   declarations: [ShowgridComponent],
   providers: [],
})

export class ShowgridComponentModule {
}
