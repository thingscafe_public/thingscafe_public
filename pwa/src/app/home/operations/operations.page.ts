//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { Component, OnInit } from '@angular/core';
import { Cache, CacheService} from 'ionic-cache-observable';

@Component({
  selector: 'app-operations',
  templateUrl: './operations.page.html',
  styleUrls: ['./operations.page.scss'],
})
export class OperationsPage implements OnInit {

	private cache: Cache<any[]>;
	constructor() { }

	ngOnInit() {
	}
		
	onRefresh(refresher): void {
  // Check if the cache has registered.
	  if (this.cache) {
		  this.cache.refresh().subscribe(() => {
		  // Refresh completed, complete the refresher animation.
		  refresher.complete();
	  }, (err) => {
		  // Something went wrong!
		  // Log the error and cancel refresher animation.
		  console.error('Refresh failed!', err);
		  refresher.cancel();
		  });
	  } else {
		// Cache is not registered yet, so cancel refresher animation.
		refresher.cancel();
	  }
	}

	async handle_operation(action:string, data?:any) {
	}

}
