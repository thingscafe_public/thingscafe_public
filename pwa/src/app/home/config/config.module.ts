//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConfigPage } from './config.page';
import { SchemaPage } from './schema/schema.page';
import { SchemaPageModule } from './schema/schema.module';
import { SharedServicesModule } from  '../../services/sharedservices.module';

const routes: Routes = [
  {
    path: '',
    component: ConfigPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
	IonicModule,
	SchemaPageModule,
	SharedServicesModule, 
    RouterModule.forChild(routes)
  ],
  entryComponents: [SchemaPage],
  declarations: [ConfigPage, SchemaPage]
})
export class ConfigPageModule {}
