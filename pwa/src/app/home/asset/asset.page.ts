//   Copyright 2020 thingscafe.net
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

import { environment, tcapp } from '../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { } from '@angular/core';
import { ActivatedRoute, Router, RouterEvent, NavigationEnd } from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { EntityService, entity_info} from '../../services/entity.service';
import { AssetService, asset_info} from '../../services/asset.service';
import { ProvisionService, push_config_input, asset_operation_input } from '../../services/provision.service';
import { ActionSheetController, ToastController, LoadingController, AlertController, NavController, ModalController } from '@ionic/angular';
import { Cache, CacheService} from 'ionic-cache-observable';
import { DataqueryService, data_query_input, data_query_response} from '../../services/dataquery.service';
import { StatsqueryService, stats_query_input, stats_query_response} from '../../services/statsquery.service';
import { Observable } from  'rxjs/Rx';
import { take, map, filter } from 'rxjs/operators';
import {AssetFillPage} from './asset-fill/asset-fill.page';
import { ShowdataComponent } from '../../vendor-components/showdata/showdata.component';
import { ShowgridComponent } from '../showgrid/showgrid.component';
import { ShowchartsComponent } from '../showcharts/showcharts.component';


class cb_params {
	input:any;
	service_func:Function;
}

export class sync_time {
    id	:string;
    date	:Date;

	public get_id() {
		return (this.id);
	}

	public get_time() {
		return (this.date);
	}

	constructor(values: Object = {}) 
	{  
		Object.assign(this, values);  
	}
}


@Component({
  selector: 'app-asset',
  templateUrl: './asset.page.html',
  styleUrls: ['./asset.page.scss'],
})

export class AssetPage implements OnInit {
	loadinganimation:any;
	private action:string;
    public asset_types:any[];
	//public data$: Observable<entity_info[]>;   
	public data$: Observable<asset_info[]>;   
	private cache: Cache<any[]>;
	private keys : string[];
	private current_entity:string;
	private controller_asset_id:string;
	private current_entity_type:number;
	public view:string;
	public title:string;
	private mypriv:string;
	private assets:any[];
	private asset_options:string[];
    carray:string[];
	private chart_data:any;
	public show_desc:boolean;

	constructor(private entitysvc:EntityService,
		private assetsvc: AssetService,
		private cachesvc: CacheService,
		private provsvc: ProvisionService,
		private dqsvc: DataqueryService,
		private statssvc: StatsqueryService,
	    private global : GlobalService, 
		private alertCtrl: AlertController, 
		private modalCtrl: ModalController,
		private toastCtrl: ToastController,
		private loadCtrl: LoadingController,
		private actionCtrl: ActionSheetController,
		private router: Router,
		private aroute: ActivatedRoute,
		private navCtrl:NavController) {
		this.title = "Managed Assets";
		this.carray = [];
		this.assets = [];
		this.chart_data = new Map();
		this.show_desc = false;
	}

	set_desc(val:boolean) {
	  this.show_desc = val;
	}

	ngOnInit() {
		this.ViewLoad();
	}

/*
	getmoredata = function(data:any, cb_arg:any, count:number):Observable<any> {

		if (cb_arg.input.data_type == "events") {

			let di = cb_arg.input;

			//di.start_time = this.global.get_UTC_start_of_day();
			di.start_time = this.global.get_UTC_start_of_week();
			di.data_type = data.data_type;
			di.entity_name = ename;
			di.resource = data.id;
			di.foptions = new Map();
			di.foptions["limit"] = 20;
			di.severity = ["info"];
			di.pcontext = data.pcontext;
			di.foptions["limit"] = count;

			return (cb_arg.service_func(di));
		}
	};
*/

	ionViewDidLoad() {
		console.log("ionViewDidLoad");
		this.ViewLoad();
	}

	ViewLoad() {
		console.log("ViewLoad");
		this.entitysvc.ready()
	      .pipe(take(2),)
		  .subscribe(resp => {
		this.asset_types = this.entitysvc.asset_types.map(atype => {
			  var operations:string[] = [];
			  let kv_info = atype.kv;
			  if (kv_info['operations']) {
				  operations = JSON.parse(kv_info['operations']);
			  }
			  let img = "assets/img/" + atype.name + ".png"
			  let chart_file = atype.name + "_charts.json";
			  this.global.load_file("assets/" + chart_file).subscribe(filedata =>
				{
					this.chart_data[atype.name] = filedata;
				});
			  return ({...atype, operations : operations, image : img});
		});
		this.aroute.params.subscribe(params => {
			console.log(params);
			if (params['id'] && params['type']) {
				this.load_linked_asset_page(params['id'], params['type']);
				if (params['type'] == "owned") {
					this.current_entity = params['id'];
					this.controller_asset_id = params['id'];
				} else {
					this.current_entity = params['entity_id'];
					this.controller_asset_id = params['id'];
				}
			}
			if (params['priv']) 
				this.mypriv = params['priv'];
			if (params['context']) {
			  this.carray = params['context'].split(',');
			}
		});
	  });
	}

	ionViewWillEnter(){
		console.log("ionViewWillEnter");
	}

	ionViewDidEnter(){
		console.log("DidEnter");
	}

	ionViewWillLeave(){
		console.log("ionViewWillLeave");
	}

	ionViewDidLeave(){
		console.log("ionViewDidLeave");

		if (this.action == "viewProfiles" || this.action == "editProfiles") {
			this.carray.pop();
		}
		this.carray.pop();
	}

	private create_asset(data:any):Observable<any>{

		var asset:asset_info = new asset_info(data);
		return this.assetsvc.create_asset(asset, this.action == "Modify");
	}

	load_linked_asset_page(id?:string, atype?:string) {

	    if (atype == "owned") 
			this.current_entity = id;
		else 
			this.controller_asset_id = id;

		this.assetsvc.ready().subscribe(resp => {
			if (resp == true) {
				this.load_asset_page(id, atype);
			}
		});
	}

	private load_asset_page(id?:string, view?:string) {
		if (id == null && view == null) {
			if (this.view == "owned") 
				id = this.current_entity;
			else 
				id = this.controller_asset_id;
			view = this.view;
		} else if (view == null) {
		  view = "owned";
		  this.view = "owned";
		} else {
		  this.view = view;
		}

		const dataObservable = this.assetsvc.get_assets(
		  id, view)
		  .map(response => {
			  if (! response || ! response.length) {
				  return ([]);
			  }
			  this.assets = response;
			  return response.map((item) => {
				  if (((view == "owned") && (item.root_asset_flag)) || 
					  (view == "owned") || 
					  (view == "controlled")) {

					  var asset_type_name:string;
					  var operations:string[] = [];

					  let asset_type_details = this.asset_type_details(item.asset_type);
					  asset_type_name = asset_type_details.name;
					  operations = asset_type_details.operations;
					  let img = asset_type_details.image;
					  return ({...item, type : asset_type_name, operations : operations, image : img});
				  }
			  });
		});
		this.cachesvc.register(view + 'assets' + id, dataObservable)
		.subscribe((cache) => {
		  this.cache = cache;
		  this.data$ = this.cache.get$;
		  // Refresh the cache immediately.
		  this.cache.refresh().subscribe();
		});
	}

	public asset_type_details(id:string):any {
		if (this.asset_types) {
			for(let item of this.asset_types ) {
				if (item.id == id) {
					return item;
				}
			}
		} else {
			return null;
		}
	}

	onRefresh(refresher): void {
  // Check if the cache has registered.
		this.assetsvc.ngOnInit();
		this.assetsvc.ready().subscribe(resp => {
		  if (this.cache) {
			  this.cache.refresh().subscribe(() => {
				  // Refresh completed, complete the refresher animation.
				  refresher.target.complete();
			  }, (err) => {
				  // Something went wrong!
				  // Log the error and cancel refresher animation.
				  console.error('Refresh failed!', err);
				  refresher.target.cancel();
				  });
		  } else {
			// Cache is not registered yet, so cancel refresher animation.
			refresher.target.cancel();
		  }
		});
		setTimeout(() => {
		  refresher.target.complete();
		}, 2000);
	}

	labelClick(e, arg1, arg2) {
	  console.log(e.target);
	  var carray:any; 

	  if (this.carray.length > 0) {
		carray = this.carray;
		carray.push(arg1.name);
	  } 

	  this.navCtrl.navigateForward(['/home/asset', { id: arg1.id, type: "controlled", entity_id:arg1.entity_id, priv:this.mypriv , context:carray}]);
	}

	async ActOnSelect(type:any, data:any) {
	  var actionSheet:any;
	  if (data.isChecked ) {
		let buttons = [];
		if (data.root_asset_flag) {
		  if (this.mypriv === "SPAdmin") {
			buttons.push({
				  text: 'Configure SMS',
				  icon: 'notifications',
				  handler: () => {
					console.log('Configure SMS clicked');
					this.handle_operation('operation:SMSconf', type, data);
				  }
			});
			buttons.push({
				  text: 'Push Config',
				  role: 'destructive',
				  icon: 'sync',
				  handler: () => {
					console.log('Sync clicked');
					this.handle_operation('Sync', type, data);
				  }
			});
		  }
		  buttons.push({
				text: 'Push User Config',
				icon: 'arrow-dropright-circle',
				handler: () => {
				  console.log('UserSync clicked');
				  this.handle_operation('UserSync', type, data);
				}
		  });
		  buttons.push({
				text: 'Show Alarms',
				icon: 'glasses',
				handler: () => {
				  console.log('Show Events clicked');
				  this.handle_data('alarms', type, data);
				}
		  });
		  buttons.push({
				text: 'Show Events',
				icon: 'glasses',
				handler: () => {
				  console.log('Show Events clicked');
				  this.handle_data('events', type, data);
				}
		  });
		}
		buttons.push({
			  text: 'Dashboard',
			  icon: 'speedometer',
			  handler: () => {
				console.log('Show Dashboard clicked');
				this.show_dashboard('counters', type, data);
			  }
		});
		for (let operation of type.operations) {
		  let icon = ""
		  let op = "operation:"
		  if (operation.includes("Start")) {
			icon = 'play';
		  } else if (operation.includes("Stop")) {
			icon = 'pause';
		  } else if (operation.includes("Controller")) {
			if (this.mypriv !== "SPAdmin") {
				continue;
			}
			icon = 'rewind';
		  } else if (operation.includes("Reset")) {
			icon = 'rewind';
		  } else if (operation.includes("Get")) {
			icon = 'glasses';
			op = "getoperation:"
		  } else {
			icon = 'eye';
			op = "getoperation:"
		  }
		  buttons.push({
			  text: operation,
			  icon: icon,
			  handler: () => {
				console.log(operation + " selected");
				this.handle_operation(op+operation, type, data);
			  }
			});
		}
		if (buttons.length > 0) {
		  buttons.push({
			  text: 'Cancel',
			  icon: 'close',
			  role: 'cancel',
			  handler: () => {
				console.log('Cancel clicked');
				actionSheet.dismiss();
			  }
		  });
		  actionSheet = await this.actionCtrl.create({
			header: 'Operations',
			buttons: buttons});
		  await actionSheet.present();
		  await actionSheet.onWillDismiss();
		  data.isChecked = false;
		}
	  }
	}


	async show_dashboard(action:string, asset_tinfo?:any, data?:any) {

		let ename = this.carray[this.carray.length - 2];

		let di = new(stats_query_input);

		if (data.root_asset_flag) {
			ename = this.carray[this.carray.length - 1];
			di.resource = data.id;
		} else {
			di.resource = data.parent_asset_id;
		}

		//di.start_time = this.get_UTC_start_of_day();
		di.start_time = this.global.get_UTC_start_of_week();
		di.end_time = this.global.get_UTC_time();
		di.data_type = action;
		di.aggregation_type = 'cnts1h'
		di.entity_name = ename;
		if (asset_tinfo.root_asset_flag) {
			// Kludge for single controller issue . Need to handle differently
			di.asset_name =  asset_tinfo.name + ":C1"
		} else {
			di.asset_name = asset_tinfo.name + ":" + data.name
		}
			//di.resource = data.id;
		di.foptions = new Map();
		//di.foptions["limit"] = 10;
		di.foptions["allow_filtering"] = true;

		let cb_arg = new(cb_params);
		cb_arg.input = di;
		cb_arg.service_func = this.statssvc.get_stats_by_asset.bind(this.statssvc);
		//cb_arg.service_func = this.statssvc.get_stats_by_entity.bind(this.statssvc);
		const modal = await this.modalCtrl.create({
			component: ShowchartsComponent,
			cssClass: 'my-custom-modal-css',
			componentProps: {resource_type:asset_tinfo.name, charts_input:this.chart_data[asset_tinfo.name], getmore_cb:this.global.getmoredata, cb_arg:cb_arg}
		});
		await modal.present();
		await modal.onDidDismiss();
	}

	async handle_data(action:string, asset_tinfo?:any, data?:any) {

		if (data.root_asset_flag) {

			let ename = this.carray[this.carray.length - 1];

			let di = new(data_query_input);

			//di.start_time = this.get_UTC_start_of_day();
			di.start_time = this.global.get_UTC_start_of_week();
			di.end_time = this.global.get_UTC_time();
			if (action == "alarms") {
				di.data_type = "events";
			} else {
				di.data_type = action;
			}
			di.entity_name = ename;
			di.resource = data.id;
			di.foptions = new Map();
			di.foptions["limit"] = 10;

			if (action == "events") {
				di.severity = ["info"];
			} else {
				di.severity = ["minor", "major", "warning", "critical", "clear"];
			}

			let cb_arg = new(cb_params);
			cb_arg.input = di;
			cb_arg.service_func = this.dqsvc.get_data_by_asset.bind(this.dqsvc);
			const modal = await this.modalCtrl.create({
				component: ShowgridComponent,
				cssClass: 'my-custom-modal-css',
				componentProps: {input_data:null, getmore_cb:this.global.getmoredata, cb_arg:cb_arg, title:action}
			});
			await modal.present();
			await modal.onDidDismiss();
		}
	}

	async handle_operation(action:string, asset_tinfo?:any, data?:any) {
	  var carray:any; 

	  if (this.carray) {
		carray = this.carray;
	  } 
	  this.action = action;

	  if (action == "Profiles") {
		  carray.push(asset_tinfo.name);
		  this.navCtrl.navigateForward(['/config', {entity_id:this.current_entity, atype_id: asset_tinfo.id, controller_id:this.controller_asset_id , context:carray}]);
	  } else if (action == "viewProfiles" || action == "editProfiles") {
		  carray.push(asset_tinfo.name);
		  if (data.root_asset_flag) {
			  carray.push(data.description);
		  } else {
			  carray.push(data.name);
		  }
		  this.navCtrl.navigateForward(['/config', {entity_id:this.current_entity, atype_id: asset_tinfo.id, asset_id:data.id, controller_id:this.controller_asset_id, context:carray }]);
	  } else if (action.includes("operation")) {
		  this.loadinganimation = this.present_loading("Performing " +  action, -1);
		  let l = new(asset_operation_input)
		  l.asset_id = data.id;
		  l.site_id = data.entity_id;
		  l.access_token = this.entitysvc.get_token_touse();
		  l.operation = action;
		  l.debug = this.global.debug;
		  if (action.includes("getoperation")) {
			  l.debug = true;
		  }
		  this.provsvc.asset_operation(l).subscribe(resp1 => {
			var header:string;
			this.loadinganimation.then(loading => {
				loading.dismiss();
			});
			header = action + ' succeeded';
			let message = "";
			if (action.includes("getoperation")) {
				  let title = this.carray[carray.length - 1] + "/" + data.description;
			}
			if (resp1.body) {
				for (let cresp of resp1.body.response) {
					message += cresp.name + ":" + cresp.desc + "\n"
					if (cresp.cmds != null) {
						for (let msg of cresp.cmds) {
							message = message + msg + "\n"; 
						}
					}
					if (cresp.replies != null && cresp.replies[0] != "Testing") {
							let act = action.split(':');
							let title = this.carray[carray.length - 1] + "/" + data.description;
						for (let msg of cresp.replies) {
							message = message + msg + "\n"; 
						}
					}
				}
			}

			let buttons = []
			buttons.push(
				{
				  text: 'Dismiss',
				  role: 'cancel',
				  cssClass: 'secondary',
				  handler: () => {
					console.log('Confirm Cancel');
				  }
				});
			buttons.push({
				  text: 'Show',
				  handler: async () => {
				  if (resp1.body) {
						  let title = this.carray[carray.length - 1] + "/" + data.description;
						  let cresp = resp1.body.response[0];
						  let rdata = cresp.replies[0];
						  this.global.app_data.next(rdata);
						  const modal = await this.modalCtrl.create({
							  component: ShowdataComponent,
							  componentProps: {json_data:rdata, title:title, action:action, debug:l.debug, message:message}
						  });
						  await modal.present();
						  await modal.onDidDismiss();
				  } else {
						  if (l.debug) {
							  this.presentToast("Commands/Response", message);
						  }
				  }
				}
			  });
			  this.alertCtrl.create({
				  header: header,
				  buttons: buttons
			  }).then(alert => alert.present());
			},
			e => {
				this.present_error(e, action);
			});
	  } else if (action == "Sync") {
			var header:string;
			this.loadinganimation = this.present_loading("Performing " +  action, -1);
			let l = new(push_config_input);
			l.site_id = data.entity_id;
			l.asset_id = data.id;
			l.access_token = this.entitysvc.get_token_touse();
			l.debug = this.global.debug;

			this.provsvc.push_config(l, false).subscribe(resp1 => {
			    var header:string;
			    if (resp1.body && resp1.body.status == "Done") {
				  header = action + ' succeeded';
				} else if (resp1.body && resp1.body.status == "Tryagain") {
				  header = action + ' took too long. Push again to complete';
				}
				data.last_config_sync = Date.now();
				let message = "";
				if (l.debug && resp1.body) {
					for (let cresp of resp1.body.response) {
						message = cresp.desc + "\n"
						if (cresp.cmds != null) {
							for (let msg of cresp.cmds) {
								message = message + msg + "\n"; 
							}
						}
						if (cresp.replies != null) {
							for (let msg of cresp.replies) {
								message = message + msg + "\n"; 
							}
						}
					}
				}
				this.loadinganimation.then(loading => {
					loading.dismiss();
				});

			    this.present_alert(header, "", message);
			},
			e => {
					this.present_error(e, action);
			});
		} else if (action == "UserSync") {
			var header:string;
			this.loadinganimation = this.present_loading("Performing " +  action, -1);
			let l = new(push_config_input);
			l.site_id = data.entity_id;
			l.asset_id = data.id;
			l.access_token = this.entitysvc.get_token_touse();
			l.debug = this.global.debug;

			this.provsvc.push_user_config(l, false).subscribe(resp1 => {
			    var header:string;
			    if (resp1.body && resp1.body.status == "Done") {
				  header = action + ' succeeded';
				} else if (resp1.body && resp1.body.status == "Tryagain") {
				  header = action + ' took too long. Push again to complete';
				}

				data.last_userconfig_sync = Date.now();
				let message = "";
				if (l.debug && resp1.body) {
					for (let cresp of resp1.body.response) {
						message = cresp.desc + "\n"
						if (cresp.cmds != null) {
							for (let msg of cresp.cmds) {
								message = message + msg + "\n"; 
							}
						}
						if (cresp.replies != null) {
							for (let msg of cresp.replies) {
								message = message + msg + "\n"; 
							}
						}
					}
				}
				this.loadinganimation.then(loading => {
					loading.dismiss();
				});
			    this.present_alert(header, "", message);
			},
			e => {
					this.present_error(e, action);
			});
	  } else if (action == "Delete") {
		let children_present = false;
		if (data.root_asset_flag) {
			children_present = this.check_for_child_assets(data.id);
			if (children_present) {
				const alert = await this.alertCtrl.create({
				  header: 'Warning!',
				  message: 'Message <strong>Please delete child assets first</strong>!!!',
				  buttons: [
					{
					  text: 'Okay',
					  role: 'okay',
					  handler: () => {
						console.log('Confirm Okay');
						// Call delete profile here
						return true;
					  }
					}
				  ]
				});
				await alert.present();
				let resp = await alert.onDidDismiss();
				return;
			}
		} 
		const alert = await this.alertCtrl.create({
			  header: 'Warning!',
			  message: 'Message <strong>Are you sure you want to delete ?</strong>!!!',
			  buttons: [
				{
				  text: 'Cancel',
				  role: 'cancel',
				  cssClass: 'secondary',
				  handler: (blah) => {
					console.log('Confirm Cancel: blah');
					return true;
				  }
				}, {
				  text: 'Okay',
				  role: 'okay',
				  handler: () => {
					console.log('Confirm Okay');
					// Call delete profile here
					return true;
				  }
				}
			  ]
		});
		await alert.present();
		let resp = await alert.onDidDismiss();
		console.log('Delete :', resp.role, data.id);  
		if (resp.role == 'okay') {
			this.loadinganimation = this.present_loading("Performing " +  action, 5);
			this.assetsvc.delete_asset(data)
			  .subscribe(resp1 => {
				this.loadinganimation.then(loading => {
					loading.dismiss();
				});
				if (this.cache) {
					this.cache.refresh().subscribe();
				}
			},
			e => {
				this.present_error(e, action);
			});
		}
		return;
	  } else if (action == "Modify") {
		  this.action = action;
		  this.presentModal(data, asset_tinfo);
	  } else if (action == "Copy") {
		  this.action = action;
		  delete(data.id);
		  this.presentModal(data, asset_tinfo);
	  } else if (action == "Add") {
		  this.action = action;
		  this.presentModal(data, asset_tinfo);
	  } else if (action == "Apply") {
		  this.action = action;
		  //  this.applyview(data);
	  }
	}

	check_for_child_assets(root_asset_id:string){
		let children_present:boolean = false;
		for (let asset of this.assets) {
			if (asset.parent_asset_id == root_asset_id) {
				children_present = true;
				return children_present;
			}
		}

		return children_present;
	}

	async presentToast(header:string, msg:any) {
		const toast = await this.toastCtrl.create({
		  message: header + '\n' + msg,
		  color:"dark",
		  cssClass:"toast",
		  showCloseButton: true,
		  translucent: true,
		  duration : 60000,
		  position : 'bottom'
		});
		toast.present();
	}

	async presentModal(asset:any, asset_tinfo?:any) {

	  if (asset) 
		  console.log("Selected option:", asset['asset_type'], this.action);

	  const modal = await this.modalCtrl.create({
		  component: AssetFillPage,
		  componentProps: {asset:asset, atypinfo:asset_tinfo, asset_types:this.asset_types, entity_id:this.current_entity, action:this.action, controller_asset_id:this.controller_asset_id,assets:this.assets}
	  });

	  const mapToObj = m => {
		  return Array.from(m).reduce((obj, [key, value]) => {
			  obj[key] = JSON.stringify(value);
			  return obj;
		}, {});
	  };

	  let resp = await modal.present();
	  let data = await modal.onDidDismiss();
	  console.log(this.action, ' DATA:', data.data);  
	  if (data.data) {
		  let collections = data.data.collmap;
		  if (collections) {
			  let coll_map = new Map(collections.map(obj => [ obj.asset_type_name, obj]));
			  let temp:any = mapToObj(coll_map);
			  data.data.collmap = JSON.stringify(temp);
		  }
		  this.loadinganimation = this.present_loading("Performing " +  this.action, 5);
		  this.create_asset(data.data).subscribe(resp1 => {
			this.loadinganimation.then(loading => {
				loading.dismiss();
			});
			if (this.cache) {
			  this.cache.refresh().subscribe();
			}
		  },
		  e => {
			  this.present_error(e, this.action);
		  });
	  }
	  return(resp);
	}

	async present_error(e:any, action?:string) {
		console.error(e)

		let message = "";
	    let emsg = e;
		if ( e.response) {
		  for (let cresp of e.response) {
			  message = cresp.desc + "\n"
			  if (cresp.cmds != null) {
				  for (let msg of cresp.cmds) {
					  message = message + msg + "\n"; 
				  }
			  }
			  if (cresp.replies != null) {
				  for (let msg of cresp.replies) {
					  message = message + msg + "\n"; 
				  }
			  }
		  }
		  emsg = e.status;
		}

		let header = action + ' failed. ';
		if (e.status == "Tryagain") {
			header += "Please correct error and try again"
		}
		this.loadinganimation.then(loading => {
			loading.dismiss();
		});

		this.present_alert(header, emsg, message);
	}

    async present_alert(header:string, msg:string, message?:string) {
	  let buttons = []
	  buttons.push(
		  {
			text: 'Dismiss',
			role: 'cancel',
			cssClass: 'secondary',
			handler: () => {
			  console.log('Confirm Cancel');
			}
		  }
	  );
	  if (this.global.debug && message != "") {
		 buttons.push({
				text: 'Show',
				handler: () => {
					this.presentToast("Commands/Response", message);
				}
			}
		 );
	  }

	  this.alertCtrl.create({
		  header: header,
		  message: msg,
		  buttons: buttons
	  }).then(alert => alert.present());
	}

	async present_loading(message:string, duration:number) {

	  if (duration > 0) {
		  const loading = await this.loadCtrl.create({
			message: message,
			duration: duration * 1000,
			spinner: 'crescent'
		  });
		  await loading.present();
		  return loading;
	  } else {
		  const loading = await this.loadCtrl.create({
			message: message,
			duration: 200000,
			spinner: 'dots'
		  });
		  await loading.present();
		  return loading;
	  }
	 
	}
}
