import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowchartsComponent } from './showcharts.component';

describe('ShowchartsComponent', () => {
  let component: ShowchartsComponent;
  let fixture: ComponentFixture<ShowchartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowchartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowchartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
