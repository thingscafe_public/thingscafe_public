# !/bin/bash
#   Copyright 2020 thingscafe.net
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http:#www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set -x
cd ~/pwa
rm tcafe
# The following are required to create index2.hbs which is used to 
# provide white labelling for the PWA
cp src/index.hbs src/index.html
ionic build --prod --verbose
cp src/index.html.local src/index.html
cp www/index.html www/index2.hbs
sed 's/{{orgName}}/tcafe/g' www/index2.hbs > www/index.html
ln -s www tcafe
tarfile="tcafe.tar.gz-$(date +'%m%d%y')"
echo "Press Enter to continue"
read a
tar -cvzf /tmp/$tarfile tcafe/*
echo "Done $tarfile"

